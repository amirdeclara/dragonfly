//
//  HTTPSharedClientExt.m
//  dragonfly
//
//  Created by Amirul Islam on 3/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "HTTPSharedClientExt.h"

@implementation HTTPSharedClientExt

@synthesize accessToken = _accessToken;

#pragma mark - API

+ (id)sharedClient
{
    static dispatch_once_t onceToken;
    static id sharedMediaServer = nil;
    
    dispatch_once( &onceToken, ^{
        sharedMediaServer = [[[self class] alloc] init];
    });
    
    return sharedMediaServer;
}

//- (void) setAccessToken:(NSString *)accessToken
//{
//    _accessToken = accessToken;
//    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:ACCESS_TOKEN_KEY];
//}
//
//- (NSString *) accessToken
//{
//    return [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
//}

- (AFHTTPRequestOperation *)GET:(NSString *)URLString
                     parameters:(id)parameters
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.shareActionGroup"];
    
    NSString *accessToken = [shared objectForKey:@"ACCESS_TOKEN"];
    
    //if(![self.manager.requestSerializer valueForKey:@"DeclaraAuthorization"]) {
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", accessToken] forHTTPHeaderField:AUTHORIZATION_HEADER];
    
    //}
    return [self.manager GET:URLString parameters:parameters success:success failure:failure];
}

- (AFHTTPRequestOperation *)POST:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    //if(![self.manager.requestSerializer valueForKey:@"DeclaraAuthorization"]) {
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    self.manager.requestSerializer = requestSerializer;
    
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",self.accessToken] forHTTPHeaderField:AUTHORIZATION_HEADER];
    //}
    
    return [self.manager POST:URLString parameters:parameters success:success failure:failure];
    
}

- (AFHTTPRequestOperation *)DELETE:(NSString *)URLString
                        parameters:(id)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    self.manager.requestSerializer = requestSerializer;
    [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",self.accessToken] forHTTPHeaderField:AUTHORIZATION_HEADER];
    return [self.manager DELETE:URLString parameters:parameters success:success failure:failure];
}


- (id)init;
{
    if ( ( self = [super init] ) )
    {
        self.manager = [[AFHTTPRequestOperationManager alloc] init];
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 2;
    }
    
    return self;
}

@end
