//
//  ShareViewController.m
//  dragonflyShareExt
//
//  Created by Amirul Islam on 3/9/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ShareViewController.h"
#import "CollectionTableViewCell.h"
#import "Collection.h"
#import "HTTPSharedClientExt.h"

@interface ShareViewController ()

@end

@implementation ShareViewController


#define API_BASE_PATH @"https://dev-840.int.declara.com/api"
#define AUTHORIZATION_HEADER @"Authorization"
#define ACCESS_TOKEN_KEY @"thetokenkey"
#define CURRENT_USER_KEY @"theuserkey"
#define UUID_TOKEN_KEY @"theuuidkey"

/*
- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

- (void)didSelectPost {
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
}

- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    return @[];
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapBackground:)];
    [self.view addGestureRecognizer:gesture];
    
    [self loadCollectionData];
   // [self loadData];
}


#pragma mark - Events
-(void) didTapBackground:(UITapGestureRecognizer *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)createCollectionButtonPressed:(id)sender {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Create New Collection" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *alertField = (UITextField *)[alert textFields].firstObject;
    }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:saveAction];
    [alert addAction:cancelAction];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Collection Name";
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        textField.textAlignment = NSTextAlignmentCenter;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)collectionPressed:(UIButton *)sender
{
    
    //TODO: pass the selected collection back to the webview
    // [self addCollection];
    
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if(self.collections.count == 0) {
        return 0;
    }
    return self.collections.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionCell" forIndexPath:indexPath];
    //    [cell.collectionButton setTitle:[self.collections objectAtIndex:indexPath.row][@"title"] forState:UIControlStateNormal];
    //    self.collectionName = [self.collections objectAtIndex:indexPath.row][@"title"];
    //    LOG(@"The self.collectionName is %@", [self.collections objectAtIndex:indexPath.row][@"title"]);
    //    [cell.collectionButton addTarget:self action:@selector(collectionPressed:self.collectionName) forControlEvents:UIControlEventTouchUpInside];
    // cell.delegate = self;
    [cell loadCollectData: [self.collections objectAtIndex:indexPath.row]];
    return cell;
}

-(IBAction)closePressed:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self closeExtension:0];
}


//-(void) loadCollectionData {
//
//
//    //AMIR  [Feeds loadFeed:feedPath accessToken: [self base64String:accessToken] completion:^(id result, NSError *error)
//    [Collection collectionWithCompletion:^(id result, NSError *error) {
//        //[self hideLoading];
//        if(error) {
//
//            return;
//        }
//        LOG(@"result %@", result);
//        self.collections = result;
//        LOG(@"hello Amir%@", self.collections);
//        [self.collectionsTableView reloadData];
//
//    }];
//
//}


-(void) loadCollectionData {
    
    
    //NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    //NSLog(@"Value from accessToken: %@", accessToken);
//    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
//    NSLog(@"Value from uuid: %@", uuid);
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.shareActionGroup"];
    NSString *uuid = [shared stringForKey:@"USER_ID"];
    [shared synchronize];
    
    NSString *collectionPath = [NSString stringWithFormat:@"%@/users/%@/collections?page=0&size=20", API_BASE_PATH, uuid];
    
    
    [[HTTPSharedClientExt sharedClient] GET:collectionPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //completion([Collection createWithDict:responseObject], nil);
        self.collections = responseObject[@"_embedded"][@"items"];
        NSLog(@"hello Amir%@", self.collections);
        [self.collectionsTableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"The error is %@", error);
    }];
    
}


-(void) loadData {
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.shareActionGroup"];
    NSString *uuid = [shared objectForKey:@"USER_ID"];
    NSString *accessToken = [shared objectForKey:@"ACCESS_TOKEN"];
    [shared synchronize];
    self.collectionsTextField.text = uuid;
    NSString *collectionPath = [NSString stringWithFormat:@"%@/users/%@/collections?page=0&size=20", API_BASE_PATH, uuid];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:collectionPath]];
    
    //[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"GET"];
    
//    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
//    NSString *escapedJSONString = [self.contentUrl stringByReplacingOccurrencesOfString:@"/" withString:@"\\/"];
//    [parent setObject:escapedJSONString forKey:@"url"];
//    [parent setObject:self.contentDescription forKey:@"description"];
//    NSError *error;
//    NSData* postData = [NSJSONSerialization dataWithJSONObject:parent
//                                                       options:kNilOptions error:&error];
//    
//    
//    [request setHTTPBody:postData];
    
    
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"Authorization"];
    //
    //NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 //[self dismissViewControllerAnimated:YES completion:nil];
                 // completion([Recommend createWithDict:jsonArray], nil);
                 self.collections = jsonArray[@"_embedded"][@"items"];
                 NSLog(@"hello Amir%@", self.collections);
                 [self.collectionsTableView reloadData];
             }
             //             completion([Profile createWithDict:jsonArray], nil);
             //            return;
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *collectionId = [self.collections objectAtIndex:indexPath.row][@"id"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    
    NSString *feedPath = [NSString stringWithFormat:@"%@/collections/%@/posts", API_BASE_PATH, collectionId];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:feedPath]];
    
    //[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    NSString *escapedJSONString = [self.contentUrl stringByReplacingOccurrencesOfString:@"/" withString:@"\\/"];
    [parent setObject:escapedJSONString forKey:@"url"];
    [parent setObject:self.contentDescription forKey:@"description"];
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:parent
                                                       options:kNilOptions error:&error];
    
    
    //[request setHTTPBody:postData];
    
    
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"Authorization"];
    //
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 [self dismissViewControllerAnimated:YES completion:nil];
                 // completion([Recommend createWithDict:jsonArray], nil);
             }
             //             completion([Profile createWithDict:jsonArray], nil);
             //            return;
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    NSString *createCollectionPath = [NSString stringWithFormat:@"%@/collections", API_BASE_PATH];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:self.collectionsTextField.text forKey:@"title"];
    [parent setObject:@"false" forKey:@"closed"];
    NSLog(@"The parent is %@", parent);
    
    [[HTTPSharedClientExt sharedClient] POST:createCollectionPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success");
        //[self dismissViewControllerAnimated:YES completion:nil];
        [self loadCollectionData];
        self.collectionsTextField.text = @"";
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"The error is %@", error);
    }];
    
    return YES;
}


- (void)closeExtension:(CGFloat)delay
{
    
    [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
    //This looks like a mess, but it's just 2 chained animations that close the extension at the end.
    /* [UIView animateWithDuration:0.2
     delay:delay
     options:UIViewAnimationOptionCurveEaseOut
     animations:^{
     self.messageLabel.alpha = 0;
     }
     completion:^(BOOL finished) {
     if (finished)
     {
     [UIView animateWithDuration:0.3
     delay:0
     options:UIViewAnimationOptionCurveEaseOut
     animations:^{
     self.messageBar.alpha = 0;
     self.shadeView.alpha = 0;
     self.messageBar.transform = CGAffineTransformMakeTranslation(0, -self.messageBar.frame.size.height);
     }
     completion:^(BOOL finished) {
     if (finished)
     {
     [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
     }
     }];
     }
     }];*/
}


@end
