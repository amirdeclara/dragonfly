
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Analytics/Core-iOS
#define COCOAPODS_POD_AVAILABLE_Analytics_Core_iOS
#define COCOAPODS_VERSION_MAJOR_Analytics_Core_iOS 1
#define COCOAPODS_VERSION_MINOR_Analytics_Core_iOS 11
#define COCOAPODS_VERSION_PATCH_Analytics_Core_iOS 0

// Analytics/Segmentio
#define COCOAPODS_POD_AVAILABLE_Analytics_Segmentio
#define COCOAPODS_VERSION_MAJOR_Analytics_Segmentio 1
#define COCOAPODS_VERSION_MINOR_Analytics_Segmentio 11
#define COCOAPODS_VERSION_PATCH_Analytics_Segmentio 0

// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 4

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
#define COCOAPODS_VERSION_MAJOR_Parse 1
#define COCOAPODS_VERSION_MINOR_Parse 6
#define COCOAPODS_VERSION_PATCH_Parse 4

// SupportKit
#define COCOAPODS_POD_AVAILABLE_SupportKit
#define COCOAPODS_VERSION_MAJOR_SupportKit 2
#define COCOAPODS_VERSION_MINOR_SupportKit 5
#define COCOAPODS_VERSION_PATCH_SupportKit 4

// TRVSDictionaryWithCaseInsensitivity
#define COCOAPODS_POD_AVAILABLE_TRVSDictionaryWithCaseInsensitivity
#define COCOAPODS_VERSION_MAJOR_TRVSDictionaryWithCaseInsensitivity 0
#define COCOAPODS_VERSION_MINOR_TRVSDictionaryWithCaseInsensitivity 0
#define COCOAPODS_VERSION_PATCH_TRVSDictionaryWithCaseInsensitivity 2

