//
//  FeedReadingListViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/17/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedReadingListViewController.h"
#import "FeedsWebViewCellTableViewCell.h"
#import "HTTPSharedClient.h"
#import "FeedsWebViewCellTableViewCell.h"
#import "CustomHeaderCell.h"
#import "Content.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImage+ImageEffects.h"
#import "NSString+declara.h"
#import "FeedReadingListViewTableViewCell.h"


@implementation FeedReadingListViewController {
    float labelWidth;
}

#define kDefaultHeaderFrame CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)

#define FONT_SIZE 16.0f
#define CELL_CONTENT_WIDTH 335.0f
#define CELL_CONTENT_MARGIN 20.0f
static CGFloat kParallaxDeltaFactor = 0.5f;
static CGFloat kMaxTitleAlphaOffset = 100.0f;
static CGFloat kLabelPaddingDist = 8.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    contentData = [[NSMutableDictionary alloc] init];
    [self loadContentData];
    
    CGRect frame = self.closeView.frame;
    frame.origin.y = self.tableView.frame.size.height - self.closeView.frame.size.height;
    self.closeView.frame = frame;
    
    [self.view bringSubviewToFront:self.closeView];
    
    //[self initialBackgroundColor];
    labelWidth = VALUE(256, 600);
    
}


-(void) initialBackgroundColor {
    
    
    if (!self.tableView.layer.mask)
    {
        maskLayer = [CAGradientLayer layer];
        
        maskLayer.locations = @[[NSNumber numberWithFloat:0.0],
                                [NSNumber numberWithFloat:0.2],
                                [NSNumber numberWithFloat:0.8],
                                [NSNumber numberWithFloat:1.0]];
        
        maskLayer.bounds = CGRectMake(0, 0,
                                      self.tableView.frame.size.width,
                                      self.tableView.frame.size.height);
        maskLayer.anchorPoint = CGPointZero;
        
        self.tableView.layer.mask = maskLayer;
    }
    
    CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    NSArray *colors;
    
    colors = @[(__bridge id)innerColor, (__bridge id)innerColor,
               (__bridge id)innerColor, (__bridge id)outerColor];
    
    ((CAGradientLayer *)self.tableView.layer.mask).colors = colors;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    //    [(ParallaxHeaderView *)self.tableView.tableHeaderView refreshBlurViewForNewImage];
    [super viewDidAppear:animated];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void) setUpUI {
    
    self.headerTitle.text = contentData[@"title"];
    
    NSString *imageUrl = contentData[@"image_url"];
    
    if (imageUrl) {
        
        [self.headerImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
        
    } else {
        //self.headerImage.image = nil;
    }
    
    
}

-(void) loadContentData{
    
    NSString *contentPath = [NSString stringWithFormat:@"%@/contents/%@", API_BASE_PATH, self.contentId];
    
    [[HTTPSharedClient sharedClient] GET:contentPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        contentData = responseObject;
        [self setUpUI];
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}

-(IBAction)closeWebView:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGSize maximumLabelSize = CGSizeMake(self.tableView.frame.size.width, MAXFLOAT);
    
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    
    NSDictionary *attr = @{NSFontAttributeName: [UIFont systemFontOfSize:15]};
    CGRect labelBounds = [[NSString stringByStrippingHTML:contentData[@"body"]] boundingRectWithSize:maximumLabelSize
                                              options:options
                                           attributes:attr
                                              context:nil];
    CGFloat height = ceilf(labelBounds.size.height);
    LOG(@"The height is %f", height);
    return height;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
 FeedReadingListViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedReadingListViewTableViewCell"];
    LOG(@"The text Amir is %@", [NSString stringByStrippingHTML:contentData[@"body"]]);
   // cell.detailLabel.text = contentData[@"body"];//[NSString stringByStrippingHTML:contentData[@"body"]];
    
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:CGRectZero];
    [label setLineBreakMode:UILineBreakModeWordWrap];
    [label setMinimumFontSize:FONT_SIZE];
    [label setNumberOfLines:0];
    [label setFont:[UIFont systemFontOfSize:FONT_SIZE]];
    [label setTag:1];

    
    [[cell contentView] addSubview:label];
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    
    CGSize size = [[NSString stringByStrippingHTML:contentData[@"body"]] sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];
    
    if (!label)
        label = (UILabel*)[cell viewWithTag:1];
    
    [label setText:[NSString stringByStrippingHTML:contentData[@"body"]]];
    [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MARGIN, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), MAX(size.height, 44.0f))];
    
    return cell;
    
}


#pragma mark - Table View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //if(scrollView == self.tableView){
    
    CGRect viewFrame = self.imageScroller.frame;
   // CGRect viewFrame = self.view.bounds;
    viewFrame.origin.y = -scrollView.contentOffset.y;
    

    
    if (scrollView.contentOffset.y > 0)
    {

        if (viewFrame.origin.y <= -150) {
            viewFrame.origin.y = -150;
            self.imageScroller.frame = viewFrame;
            self.imageScroller.scrollEnabled = NO;
            self.tableView.scrollEnabled = YES;
            self.imageScroller.clipsToBounds = NO;


        }
        else
        {
            self.imageScroller.scrollEnabled = YES;
             self.tableView.scrollEnabled = YES;
           // self.imageScroller.bounds = viewFrame;
            self.imageScroller.frame = viewFrame;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, -scrollView.contentOffset.y+ self.headerView.frame.size.height, self.tableView.frame.size.width, self.tableView.frame.size.height + scrollView.contentOffset.y);
            self.tableView.clipsToBounds = YES;
           

        }
    }
    else{
            self.imageScroller.scrollEnabled = YES;
            self.imageScroller.frame = viewFrame;
            self.imageScroller.clipsToBounds = YES;
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, scrollView.contentOffset.y+ self.headerView.frame.size.height, self.tableView.frame.size.width, self.tableView.frame.size.height + scrollView.contentOffset.y);
    }

   // }
    
    [self layoutCloseButtonForScrollViewOffset:scrollView.contentOffset];
}


- (void)layoutCloseButtonForScrollViewOffset:(CGPoint)offset
{
    if (offset.y > 0)
    {
        self.closeButton.imageView.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * offset.y * 2);
        
        if(self.closeButton.imageView.alpha <= 0.25){
            self.closeButton.imageView.alpha = 0.25;
        }
        
        
    }
    else{
        self.closeButton.alpha = 1.0;
        
    }
}




@end
