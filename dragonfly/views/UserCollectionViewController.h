//
//  UserCollectionViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCollection.h"
#import "UserCollectionHeaderTableViewCell.h"


@interface UserCollectionViewController : UIViewController <UserCollectionHeaderTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *collection;

@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *collectionId;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UILabel *collectionHeader;
@property (nonatomic, strong) IBOutlet UILabel *followerName;

@property (nonatomic, strong) IBOutlet UIView *closeView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) UserCollection *item;

@property (nonatomic, assign) ContentType contentType;

-(IBAction)closeCollection:(id)sender;

@end
