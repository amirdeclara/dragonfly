//
//  ActivityViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityTableViewCell.h"
#import "Notification.h"
#import "ProgressHUD.h"

@interface ActivityViewController ()

@end

@implementation ActivityViewController

#define CELL_HEIGHT 80;

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.notification = [[NSMutableArray alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [ProgressHUD show:@"Loading..."];
    [self loadNotificationData];
    
}

#pragma mark - methods

-(void) loadNotificationData {
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    NSLog(@"Value from accessToken: %@", accessToken);
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSLog(@"Value from uuid: %@", uuid);
    
    NSString *notificationPath = [NSString stringWithFormat:@"%@/notifications", API_BASE_PATH];
    
    NSLog(@"Value from notificationPath: %@", notificationPath);
    
    [Notification notificationWithCompletion:^(id result, NSError *error) {
        
        if(error) {
            
            return;
        }
        LOG(@"result %@", result);
        [ProgressHUD dismiss];
        self.notification = result;
        LOG(@"hello Amir%@", self.notification);
        [self.tableView reloadData];
        
    }];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notification.count;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell loadNotification:[self.notification objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [ProgressHUD show:@"Loading..."];
    LOG(@"The notification is %@", [self.notification objectAtIndex:indexPath.row][@"id"]);
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    
    NSString *notifyPath = [NSString stringWithFormat:@"%@/notifications/%@", API_BASE_PATH, [self.notification objectAtIndex:indexPath.row][@"id"]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:notifyPath]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"PUT"];
    
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"DeclaraAuthorization"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [self.notification removeObjectAtIndex:[indexPath row]];
         [self loadNotificationData];
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"jsonArray is %@", jsonArray);
                 [ProgressHUD dismiss];
                 [self loadNotificationData];
             }
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}


@end
