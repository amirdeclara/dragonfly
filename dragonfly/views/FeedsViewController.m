//
//  feedsViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedsViewController.h"
#import "FeedsTableViewCell.h"
#import "UserCollectionViewController.h"
#import "ReadingArticleViewController.h"
#import "HTTPSharedClient.h"
#import "ProgressHUD.h"
#import "Feeds.h"
//#import "FeedsWebViewController.h"

@interface FeedsViewController ()

@end

@implementation FeedsViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.feed = [[NSMutableArray alloc] init];
    isfirstTimeTransform = YES;
    [ProgressHUD show:@"Loading..."];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self loadFeedData];
    [self.collectionView reloadData];
    
}

#pragma mark - CollectionView Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(self.feed.count == 0){
        self.collectionView.hidden = YES;
        return 1;
    }
    else{
        self.collectionView.hidden = NO;
        return self.feed.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FeedsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FeedsCollectionViewCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0 && isfirstTimeTransform) {
        isfirstTimeTransform = NO;
    }else{
        cell.transform = TRANSFORM_CELL_VALUE;
    }
    if(self.feed.count > 0){
        cell.delegate = self;
        [cell loadData:[self.feed objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReadingArticleViewController *nav = [READING_STORYBOARD instantiateViewControllerWithIdentifier:@"ReadingArticleVC"];
    nav.contentId = [self.feed objectAtIndex:indexPath.row][@"_embedded"][@"item"][@"_embedded"][@"content"][@"id"];
    [self.navigationController presentViewController: nav animated:YES completion:nil];
    
}

#pragma mark Collection view scrolling

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    
    float pageWidth = 327;// + 12.5;
    
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = currentOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, 0) animated:YES];
    
    int index = newTargetOffset / pageWidth;
    
    if (index == 0) {
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
        
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }else{
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = CGAffineTransformIdentity;
        }];
        
        index --;
        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
        
        index ++;
        index ++;
        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            cell.transform = TRANSFORM_CELL_VALUE;
        }];
    }
}


#pragma mark Collection view methods

-(void) loadFeedData {
    //Load the data in the Feeds model
    
    [Feeds feedWithCompletion:^(id result, NSError *error) {
        if(error)
        {
            LOG(@"%@", error);
            return;
        }
        
        LOG(@"result %@", result);
        [ProgressHUD dismiss];
        NSArray *response = result;
        for(int i=0; i<response.count; i++)
        {
            //if([[response objectAtIndex:i][@"item_type"] isEqualToString:@"content"]){
            //self.shimmeringView.shimmering = NO;
            [self.feed addObject:[response objectAtIndex:i]];
            // }
        }
        
        [self.collectionView reloadData];
    }];
    [self.collectionView reloadData];
}


-(void) loadUserCollection : (NSString *) usedId collectionId:(NSString *)collectionId{
    
    UserCollectionViewController *nav = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"UserCollectionVC"];
    nav.userId = usedId;
    nav.collectionId = collectionId;
    [self.navigationController presentViewController: nav animated:YES completion:nil];
    
}

#pragma mark Collection view layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize mElementSize = CGSizeMake(325, self.collectionView.frame.size.height);
    return mElementSize;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,24,0,0);
}



@end
