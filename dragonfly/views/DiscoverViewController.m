//
//  discoverViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "DiscoverViewController.h"
#import "FriendRecommendViewController.h"
#import "RecommendationTableViewCell.h"
#import "Recommend.h"
#import "RecommendArticleTableViewCell.h"
#import "RecommendCollectionTableViewCell.h"
#import "RecommendPeopleTableViewCell.h"
#import "UserCollectionViewController.h"
//#import "FeedsWebViewController.h"
#import "CollectionReadingViewController.h"
#import "HTTPSharedClient.h"
#import "ProgressHUD.h"
#import "UserProfile.h"
#import "AnotherUserProfileViewController.h"


@interface DiscoverViewController () {
    float labelWidth;
    
}

@end

@implementation DiscoverViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    recommend = [[NSMutableArray alloc] init];
    self.shimmeringView = [[FBShimmeringView alloc] initWithFrame:self.headerView.bounds];
    [self.view addSubview:self.shimmeringView];
    self.shimmeringView.userInteractionEnabled = NO;
    
    self.shimmeringView.contentView = self.headerLabel;
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.shimmeringView.shimmering = YES;
    
    labelWidth = VALUE(256, 600);
    
    self.articlesLine.hidden = NO;
    self.collectionsLine.hidden = YES;
    self.peopleline.hidden = YES;
    [self.articleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.peopleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    // [self loadArticleData : RecommendationTypeArticle];
    [ProgressHUD show:@"Loading..."];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self loadArticleData : RecommendationTypeArticle];
}

-(void) goToFollowPeople {
    
    FriendRecommendViewController *controller = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"FriendRecommendVC"];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:navController
                       animated:YES
                     completion:nil];
    
    
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(recommend.count == 0){
        self.tableView.hidden = YES;
        return 1;
    }
    else{
        self.tableView.hidden = NO;
        return recommend.count;
    }
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(self.recommendationType){
        case RecommendationTypeArticle:
            return 220;
            //return [self getRowHeightForIndex:indexPath.row];
            break;
        case  RecommendationTypeCollection:
            return 310;
            break;
        case RecommendationTypePeople:
            return 80;
            break;
    }
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecommendArticleTableViewCell *articleCell;
    RecommendCollectionTableViewCell *collectionCell;
    RecommendPeopleTableViewCell *peopleCell;
    
    switch(self.recommendationType){
        case RecommendationTypeArticle:
            articleCell = [tableView dequeueReusableCellWithIdentifier:@"RecommendArticleTableViewCell"];
            articleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            if(recommend.count > 0){
                articleCell.delegate = self;
                [articleCell loadRecommendArticleData:[recommend objectAtIndex:indexPath.row]];
            }
            return articleCell;
            break;
        case  RecommendationTypeCollection:
            collectionCell = [tableView dequeueReusableCellWithIdentifier:@"RecommendCollectionTableViewCell"];
            collectionCell.selectionStyle = UITableViewCellSelectionStyleNone;
            collectionCell.delegate = self;
            if(recommend.count > 0){
                [collectionCell loadRecommendCollectionData:[recommend objectAtIndex:indexPath.row]];
            }
            return collectionCell;
            break;
        case RecommendationTypePeople:
            peopleCell = [tableView dequeueReusableCellWithIdentifier:@"RecommendPeopleTableViewCell"];
            peopleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            if(recommend.count > 0){
                peopleCell.delegate = self;
                [peopleCell loadRecommendPeopleData:[recommend objectAtIndex:indexPath.row]];
            }
            return peopleCell;
            break;
    }
    
    return nil;
    
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.recommendationType == RecommendationTypeArticle) {
        
        CollectionReadingViewController *nav = [READING_STORYBOARD instantiateViewControllerWithIdentifier:@"CollectionReadingVC"];
        nav.contentId = [recommend objectAtIndex:indexPath.row][@"_embedded"][@"item"][@"id"];
        [self.navigationController presentViewController: nav animated:YES completion:nil];
        
    }
    else if(self.recommendationType == RecommendationTypeCollection){
        UserCollectionViewController *nav = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"UserCollectionVC"];
        nav.userId = [recommend objectAtIndex:indexPath.row][@"_embedded"][@"item"][@"_embedded"][@"owners"];
        nav.collectionId = [recommend objectAtIndex:indexPath.row][@"_embedded"][@"item"][@"id"];
        [self presentViewController: nav animated:YES completion:nil];
    }
    else if(self.recommendationType == RecommendationTypePeople){
        
        [self followUser:[recommend objectAtIndex:indexPath.row][@"_embedded"][@"item"][@"id"]];
    }
    
    
}

#pragma mark - methods

-(CGFloat)getRowHeightForIndex:(NSInteger)index
{
    CGSize maximumSize = CGSizeMake(labelWidth, 10000);
    CGSize labelHeighSize = [[recommend objectAtIndex:index][@"_embedded"][@"item"][@"title"] sizeWithFont: [UIFont fontWithName:@"Helvetica" size:15.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    float height = MAX(labelHeighSize.height + 26, 220);
    NSLog(@"height: %ld, %f, %f", (long)index, labelHeighSize.height, height);
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    return height;
}


-(void) loadArticleData : (RecommendationType)recommendationType {
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    NSString *recommendPath = [NSString stringWithFormat:@"%@/users/%@/recommendations?page=0&size=100", API_BASE_PATH, uuid];
    
    NSString *recommendType = @" ";
    switch(self.recommendationType){
        case RecommendationTypeArticle:
            recommendType = @"&type=post";
            break;
        case  RecommendationTypeCollection:
            recommendType = @"&type=collection";
            break;
        case RecommendationTypePeople:
            recommendType = @"&type=user";
            break;
    }
    NSString *recommendationPath = [NSString stringWithFormat:@"%@%@", recommendPath, recommendType];
    
    [Recommend recommendWithCompletion: recommendationPath completion:^(id result, NSError *error) {
        
        if(error) {
            
            return;
        }
        self.shimmeringView.shimmering = NO;
        [ProgressHUD dismiss];
        recommend = result;
        [self.tableView reloadData];
        
    }];
    
    [self.tableView reloadData];
}

-(void) followCollection : (NSString *) collectionId {
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following", API_BASE_PATH, uuid];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:collectionId forKey:@"id"];
    [parent setObject:@"collection" forKey:@"type"];
    
    [[HTTPSharedClient sharedClient] POST:followPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

-(void) unFollowCollection : (NSString *) collectionId {
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following/%@", API_BASE_PATH, uuid, collectionId];
    
    [[HTTPSharedClient sharedClient] DELETE:followPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.tableView reloadData];
        LOG(@"Success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

-(void) loadUserCollection : (NSString *) usedId collectionId:(NSString *)collectionId{
    
    UserCollectionViewController *nav = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"UserCollectionVC"];
    nav.userId = usedId;
    nav.collectionId = collectionId;
    [self presentViewController: nav animated:YES completion:nil];
    
}

-(void) followUser : (NSString *) userId{
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following", API_BASE_PATH, uuid];
    
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:userId forKey:@"id"];
    [parent setObject:@"user" forKey:@"type"];
    
    [[HTTPSharedClient sharedClient] POST:followPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        [self loadArticleData : self.recommendationType];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}


-(void) loadUserProfile : (NSString *) usedId {
    
    AnotherUserProfileViewController *nav = [PROFILE_STORYBOARD instantiateViewControllerWithIdentifier:@"AnotherUserProfileVC"];
    nav.userId = usedId;
    [self presentViewController: nav animated:YES completion:nil];
    
}

#pragma mark - IBAction

-(IBAction) getArticles:(id)sender {
    self.articlesLine.hidden = NO;
    self.collectionsLine.hidden = YES;
    self.peopleline.hidden = YES;
    [self.articleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.peopleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.recommendationType = RecommendationTypeArticle;
    [ProgressHUD show:@"Loading..."];
    [self loadArticleData : self.recommendationType];
}

-(IBAction) getCollections:(id)sender {
    self.articlesLine.hidden = YES;
    self.collectionsLine.hidden = NO;
    self.peopleline.hidden = YES;
    [self.articleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.peopleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.recommendationType = RecommendationTypeCollection;
    [ProgressHUD show:@"Loading..."];
    
    [self loadArticleData : self.recommendationType];
}

-(IBAction) getPeople:(id)sender {
    self.articlesLine.hidden = YES;
    self.collectionsLine.hidden = YES;
    self.peopleline.hidden = NO;
    [self.articleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.peopleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.recommendationType = RecommendationTypePeople;
    [ProgressHUD show:@"Loading..."];
    [self loadArticleData : self.recommendationType];
}

@end
