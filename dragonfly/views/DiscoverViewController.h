//
//  discoverViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendCollectionTableViewCell.h"
#import "FBShimmeringView.h"
#import "RecommendArticleTableViewCell.h"
#import "RecommendPeopleTableViewCell.h"


@interface DiscoverViewController : UIViewController <RecommendCollectionTableViewCellDelegate, RecommendArticleTableViewCellDelegate, RecommendPeopleTableViewCellDelegate>{
    
    //NSArray *recommend;
    NSMutableArray *recommend;
    
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIView *articlesLine;
@property (nonatomic, strong) IBOutlet UIView *collectionsLine;
@property (nonatomic, strong) IBOutlet UIView *peopleline;
@property (nonatomic, strong) IBOutlet UIButton *articleBtn;
@property (nonatomic, strong) IBOutlet UIButton *collectionBtn;
@property (nonatomic, strong) IBOutlet UIButton *peopleBtn;
@property (nonatomic, assign) RecommendationType recommendationType;
@property (nonatomic, strong) FBShimmeringView *shimmeringView;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;

@property (nonatomic, strong) NSString *collectionFollowId;

-(IBAction) getArticles:(id)sender;
-(IBAction) getCollections:(id)sender;
-(IBAction) getPeople:(id)sender;

@end
