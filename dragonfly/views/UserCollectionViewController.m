//
//  UserCollectionViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UserCollectionViewController.h"
#import "RecommendArticleTableViewCell.h"
#import "UserCollection.h"
#import "UserCollectionTableViewCell.h"
#import "HTTPSharedClient.h"
#import "Profile.h"
//#import "FeedsWebViewController.h"
#import "CollectionReadingViewController.h"
#import "UserCollectionHeaderTableViewCell.h"
#import "ProgressHUD.h"

@interface UserCollectionViewController () {
    
    BOOL toggleIsOn;
    NSString *followString;
}

@end

@implementation UserCollectionViewController

#define CELL_HEIGHT_SMALL 80
#define CELL_HEIGHT_BIG 220

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.collection = [[NSMutableArray alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self loadUserCollectionData];
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.item.collections.count + 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return CELL_HEIGHT_SMALL;
    }
    else{
        return CELL_HEIGHT_BIG;
    }
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        UserCollectionHeaderTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"UserCollectionHeaderTableViewCell"];
        headerCell.delegate = self;
        [headerCell loadCollectionData:self.item];
        return headerCell;
    }
    else{
    UserCollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCollectionTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell loadUserCollectionData:[self.item.collections objectAtIndex:indexPath.row - 1] userImage:self.item.profileImageUrl];
    return cell;
    }
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionReadingViewController *nav = [READING_STORYBOARD instantiateViewControllerWithIdentifier:@"CollectionReadingVC"];
    nav.contentId = [self.item.collections objectAtIndex:indexPath.row][@"id"];
    [self presentViewController: nav animated:YES completion:nil];
    
}

#pragma mark - IBAction

-(IBAction)closeCollection:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark view scrolling

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView.contentOffset.y > 0)
    {
        self.closeButton.imageView.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * scrollView.contentOffset.y);
        
        if(self.closeButton.imageView.alpha <= 0.25){
            self.closeButton.imageView.alpha = 0.25;
        }
        
    }
    else{
        self.closeButton.alpha = 1.0;
        
    }
    
}

#pragma mark - ViewController methods

-(void) loadUserCollectionData {
    
    [ProgressHUD show:@"Loading..."];
    
    NSString *recommendPath = [NSString stringWithFormat:@"%@/collections/%@", API_BASE_PATH, self.collectionId];
    
    [UserCollection userCollectionWithCompletion: recommendPath completion:^(id result, NSError *error) {
        
        [ProgressHUD dismiss];
        if(error) {
            
            return;
        }
        self.collection = result;
        self.item = result;
        [self setCollectionUI];
        [self.tableView reloadData];
        
    }];
    
    [self.tableView reloadData];
}

-(void) setCollectionUI {
    
    self.collectionHeader.text = self.item.collectionTitle;
    NSMutableArray *ownerArray = [[NSMutableArray alloc] init];
    NSString *ownerStr = @"";
    NSInteger num;
    for (int i=0; i<self.item.owners.count; i++){
        NSString *own = [self.item.owners objectAtIndex:i][@"name"][@"full"];
        LOG(@"The own is %@", own);
        if(i < 2){
            [ownerArray addObject:own];
        }
        
    }
    ownerStr = [ownerArray componentsJoinedByString:@","];
    
    if(self.item.owners.count>2){
        num = self.item.owners.count - 2;
        NSString *numFollow = [NSString stringWithFormat:@"%ld", (long)num];
        self.followerName.text = [NSString stringWithFormat:@"%@ + %@", ownerStr, numFollow];//ownerStr;
    }
    else{
        self.followerName.text = [NSString stringWithFormat:@"%@", ownerStr];
    }
    
}

#pragma mark - delegate

-(void) followCollection :(NSString *) collectionId {
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following", API_BASE_PATH, uuid];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:self.collectionId forKey:@"id"];
    [parent setObject:@"collection" forKey:@"type"];
    
    [[HTTPSharedClient sharedClient] POST:followPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        //[self.tableView reloadData];
        [self loadUserCollectionData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

-(void) unFollowCollection :(NSString *) collectionId{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following/%@", API_BASE_PATH, uuid, self.collectionId];
    
    [[HTTPSharedClient sharedClient] DELETE:followPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[self.tableView reloadData];
         [self loadUserCollectionData];
        LOG(@"Success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

@end
