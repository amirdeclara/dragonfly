//
//  mainNavigationViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "MainNavigationViewController.h"
#import "UIImage+declara.h"


@interface MainNavigationViewController ()

@end

@implementation MainNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (UINavigationController *) navForViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image
{
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    vc.edgesForExtendedLayout = UIRectEdgeNone;
    vc.title = title;
    vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:[UIImage imageNamed:image] tag:1];
    vc.navigationController.navigationBarHidden = YES;
    return nav;
}

- (void) onHome:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.viewControllers = @[
                             NAVCTL([MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"feedsVC"], @"", @"readingListIcon"),
                             NAVCTL([MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"discoverVC"], @"", @"discoverIcon"),
                             NAVCTL([MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"activityVC"], @"", @"notificationsIcon"),
                             NAVCTL([PROFILE_STORYBOARD instantiateViewControllerWithIdentifier:@"profileVC"], @"", @"profileIcon"),
                             ];
    

    UIImage *imageCorner = [[UIImage alloc] init];
    [[UITabBar appearance] setBackgroundImage:[imageCorner imageFromColor:[UIColor blackColor] forSize:CGSizeMake(320, 60) withCornerRadius:IMAGE_RADIUS]];

    self.tabBar.backgroundColor = [UIColor blackColor];
    self.moreNavigationController.delegate = self;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {

}

- (BOOL)shouldAutorotate
{
    return NO;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait + UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
