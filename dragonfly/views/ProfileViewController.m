//
//  profileViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ProfileViewController.h"
#import "Profile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Recommend.h"
#import "RecommendArticleTableViewCell.h"
#import "RecommendCollectionTableViewCell.h"
#import "UIImage+declara.h"
#import "ProfileCollectionTableViewCell.h"
#import "Collection.h"
#import "ProgressHUD.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

#define CELL_HEIGHT 310

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.articlesLine.hidden = NO;
    self.collectionsLine.hidden = YES;
    [self.articleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    item = [[Profile alloc] init];
    //profileResult = [[NSMutableArray alloc] init];
    [self loadProfile];
    [self loadArticleData : RecommendationTypeArticle];
}

-(void)viewWillAppear:(BOOL)animated {
    
    LOG(@"item %@", item);
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return recommend.count;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    switch(self.recommendationType){
    //        case RecommendationTypeArticle:
    //            return 220;
    //            break;
    //        case  RecommendationTypeCollection:
    //            return 220;
    //            break;
    //        case RecommendationTypePeople:
    //            return 80;
    //            break;
    //    }
    return CELL_HEIGHT;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //RecommendArticleTableViewCell *articleCell;
    ProfileCollectionTableViewCell *collectionCell;
    
    //    switch(self.recommendationType){
    //        case RecommendationTypeArticle:
    //            articleCell = [tableView dequeueReusableCellWithIdentifier:@"RecommendArticleTableViewCell"];
    //            articleCell.selectionStyle = UITableViewCellSelectionStyleNone;
    //            [articleCell loadRecommendArticleData:[recommend objectAtIndex:indexPath.row]];
    //            return articleCell;
    //            break;
    //        case  RecommendationTypeCollection:
    collectionCell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCollectionTableViewCell"];
    collectionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [collectionCell loadProfileCollectionData:[recommend objectAtIndex:indexPath.row]];
    return collectionCell;
    //            break;
    //    }
    //
    //    return nil;
}

#pragma mark - ViewController methods

-(void) loadProfile {
    
    //    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    //    NSLog(@"Value from accessToken: %@", accessToken);
    //    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    //    NSLog(@"Value from uuid: %@", uuid);
    
    //NSString *profilePath = [NSString stringWithFormat:@"%@/users/%@/profile", API_BASE_PATH, uuid ];
    
    [Profile profileWithCompletion:^(id result, NSError *error) {
        
        if(error) {
            
            return;
        }
        LOG(@"result %@", result);
        item = result;
        [self setUpUI];
        
    }];
    
    [self.tableView reloadData];
    
}

-(void) loadArticleData : (RecommendationType)recommendationType {
    
    [ProgressHUD show:@"Loading..."];
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    //AMIR  NSString *recommendationPath = [NSString stringWithFormat:@"%@/users/%@/recommendations?page=0&size=20&type=user", API_BASE_PATH, uuid];
    // NSString *recommendPath = [NSString stringWithFormat:@"%@/users/%@/recommendations?page=0&size=20", API_BASE_PATH, uuid];
    
    NSString *recommendationPath = @" ";
    switch(self.recommendationType){
        case RecommendationTypeArticle:
            recommendationPath = [NSString stringWithFormat:@"%@/users/%@/recommendations?page=0&size=20&type=post", API_BASE_PATH, uuid];
            break;
        case  RecommendationTypeCollection:
            recommendationPath = [NSString stringWithFormat:@"%@/users/%@/collections?page=0&size=20", API_BASE_PATH, item.userId];
            break;
    }
    
    
    LOG(@"Value from notificationPath: %@", recommendationPath);
    
    [Collection collectionWithCompletion:^(id result, NSError *error) {
        [ProgressHUD dismiss];
        if(error) {
            
            return;
        }
        LOG(@"result %@", result);
        recommend = result;
        [self.tableView reloadData];
        
    }];
    
    [self.tableView reloadData];
}


-(void) setUpUI{
    
    self.firstName.text = item.fullName;
    
    self.followerLabel.text = [NSString stringWithFormat:@"%@ followers . %@ following", item.noFollowers, item.noCollections ];
    
    if (item.imageUrl) {
        
        [self.profileImage sd_setImageWithURL:[NSURL URLWithString:item.imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
        UIImage *imageCorner = [[UIImage alloc] init];
        [imageCorner circularImage: self.profileImage radius:25];
    } else {
        self.profileImage.image = nil;
    }
}

#pragma mark - IBAction

-(IBAction)getArticles:(id)sender {
    self.articlesLine.hidden = NO;
    self.collectionsLine.hidden = YES;
    [self.articleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.recommendationType = RecommendationTypeArticle;
    [self loadArticleData : self.recommendationType];
}

-(IBAction)getCollections:(id)sender {
    self.articlesLine.hidden = YES;
    self.collectionsLine.hidden = NO;
    [self.articleBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.collectionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.recommendationType = RecommendationTypeCollection;
    [self loadArticleData : self.recommendationType];
}

@end
