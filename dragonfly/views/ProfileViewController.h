//
//  profileViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"

@interface ProfileViewController : UIViewController {
    
    Profile *item;
    NSArray *profileResult;
    NSArray *recommend;
    
}


@property (nonatomic, strong) IBOutlet UILabel *firstName;
@property (nonatomic, weak) IBOutlet UIImageView *profileImage;
@property (nonatomic, strong) IBOutlet UILabel *followerLabel;
@property (nonatomic, assign) RecommendationType recommendationType;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIImageView *articlesLine;
@property (nonatomic, strong) IBOutlet UIImageView *collectionsLine;
@property (nonatomic, strong) IBOutlet UIButton *articleBtn;
@property (nonatomic, strong) IBOutlet UIButton *collectionBtn;


-(IBAction)getArticles:(id)sender;
-(IBAction)getCollections:(id)sender;

@end
