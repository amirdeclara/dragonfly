//
//  FriendRecommendViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FriendRecommendViewController.h"

@interface FriendRecommendViewController ()

@end

@implementation FriendRecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"RECOMMEND";
    
    ADD_BACK_BUTTON;

}

- (void) onBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
