//
//  CollectionAddViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "CollectionAddViewController.h"
#import "CollectionTableViewCell.h"
#import "Collection.h"

@interface CollectionAddViewController ()

@end

@implementation CollectionAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadCollectionData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)createCollectionButtonPressed:(id)sender {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Create New Collection" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *alertField = (UITextField *)[alert textFields].firstObject;
    }];
        
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:saveAction];
    [alert addAction:cancelAction];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Collection Name";
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        textField.textAlignment = NSTextAlignmentCenter;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)collectionPressed:(UIButton *)sender
{
  
    //TODO: pass the selected collection back to the webview
   // [self addCollection];
    
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
     return self.collections.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionCell" forIndexPath:indexPath];
//    [cell.collectionButton setTitle:[self.collections objectAtIndex:indexPath.row][@"title"] forState:UIControlStateNormal];
//    self.collectionName = [self.collections objectAtIndex:indexPath.row][@"title"];
//    LOG(@"The self.collectionName is %@", [self.collections objectAtIndex:indexPath.row][@"title"]);
//    [cell.collectionButton addTarget:self action:@selector(collectionPressed:self.collectionName) forControlEvents:UIControlEventTouchUpInside];
    cell.delegate = self;
    [cell loadCollectData: [self.collections objectAtIndex:indexPath.row]];
    return cell;
}

-(IBAction)closePressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) loadCollectionData {

    
    //AMIR  [Feeds loadFeed:feedPath accessToken: [self base64String:accessToken] completion:^(id result, NSError *error)
    [Collection collectionWithCompletion:^(id result, NSError *error) {
        //[self hideLoading];
        if(error) {
            
            return;
        }
        LOG(@"result %@", result);
        self.collections = result;
        LOG(@"hello Amir%@", self.collections);
        [self.collectionsTableView reloadData];
        
    }];
    
}



-(void) addCollection : (NSString *) collectionName{
    
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    
    NSString *feedPath = [NSString stringWithFormat:@"%@/collections", API_BASE_PATH];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:feedPath]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:collectionName forKey:@"title"];
   // [parent setObject:@"collection" forKey:@"type"];
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:parent
                                                       options:kNilOptions error:&error];
    
    [request setHTTPBody:postData];
    

    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"DeclaraAuthorization"];
    //
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 [self dismissViewControllerAnimated:YES completion:nil];
                 // completion([Recommend createWithDict:jsonArray], nil);
             }
             //             completion([Profile createWithDict:jsonArray], nil);
             //            return;
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}


@end
