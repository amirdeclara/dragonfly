//
//  FeedReadingListViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/17/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedsWebViewCellTableViewCell.h"
#import "FeedReadingListViewTableViewCell.h"

@interface FeedReadingListViewController : UIViewController <FeedsWebViewCellTableViewCellDelegate> {
    
    NSInteger cellHeight;
    NSIndexPath *lastIndexPath;
    NSMutableDictionary *contentData;
    NSString *hello;
    CAGradientLayer *maskLayer;
}

@property (nonatomic, retain) IBOutlet UIScrollView * imageScroller;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, strong) IBOutlet UIView *headerView;

@property(retain, readwrite) NSString *feedUrl;




@property (nonatomic, strong) NSMutableArray *feed;
@property (nonatomic, strong) NSString *contentId;
@property (nonatomic, strong) NSString *contentType;

@property (nonatomic, strong) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) IBOutlet UIView *closeView;

@property (nonatomic, retain) IBOutlet UIImageView * headerImage;
@property (nonatomic, retain) IBOutlet UILabel * headerTitle;


-(IBAction)closeWebView:(id)sender;

- (IBAction)shareButtonTapped:(id)sender;

- (IBAction)collectButtonTapped:(id)sender;

- (IBAction)likeButtonTapped:(id)sender;

@property (strong) FeedReadingListViewTableViewCell *cellPrototype;

@end
