//
//  CollectionAddViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionTableViewCell.h"

@interface CollectionAddViewController : UIViewController <AddCollectionTableViewCellDelegate>

@property (nonatomic, strong) NSArray* collections;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *collectionsView;

@property (weak, nonatomic) IBOutlet UIButton *createCollectionButton;

@property (weak, nonatomic) IBOutlet UITableView *collectionsTableView;

-(IBAction)createCollectionButtonPressed:(id)sender;

-(IBAction)closePressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *shadeButton;

@property (weak, nonatomic) IBOutlet UIView *collectionsInnerRoundedView;

@property (nonatomic, strong) NSString *collectionName;

@end
