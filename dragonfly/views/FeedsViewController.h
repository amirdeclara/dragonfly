//
//  feedsViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Feeds.h"
#import "FeedsCollectionViewCell.h"


@interface FeedsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, FeedsCollectionViewCellDelegate, UIScrollViewDelegate>{
    BOOL isfirstTimeTransform;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) UIViewController* viewController;
@property (nonatomic, strong) NSMutableArray *feed;
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;

@property (nonatomic, strong) IBOutlet UIView *headerView;

@end
