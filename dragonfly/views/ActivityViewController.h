//
//  ActivityViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityViewController : UIViewController {
    
//    NSMutableArray *notification;
}

@property (nonatomic, strong) NSMutableArray *notification;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
