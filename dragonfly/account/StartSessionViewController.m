//
//  StartSessionViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/24/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "StartSessionViewController.h"
#import "MainNavigationViewController.h"
#import "SessionManager.h"

@interface StartSessionViewController ()

@end

@implementation StartSessionViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

#pragma mark - IBAction

- (IBAction)button_TouchUp:(UIButton *)sender
{
    //Check for session. If the session exists, invoke the main view and if not invoke the Declara login screen.
    if([SessionManager shared].loggedIn == YES){
        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.shareActionGroup"];
        NSString *uuid = [shared stringForKey:@"USER_ID"];
        [shared synchronize];
        LOG(@"The ACCESS_TOKEN is %@", [shared valueForKey:@"ACCESS_TOKEN"]);
        MainNavigationViewController *vc = [[MainNavigationViewController alloc] init];
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    else{
    
    UINavigationController *nav = [ACCOUNT_STORYBOARD instantiateViewControllerWithIdentifier:@"CreateSessionNav"];
    [self presentViewController:nav animated:YES completion:nil];
    }

}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     MainNavigationViewController *vc = [[MainNavigationViewController alloc] init];
     [self presentViewController:vc animated:YES completion:nil];
 }

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return 44.0f;

}

#pragma mark - Orientation

- (BOOL)shouldAutorotate
{
    return NO;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait + UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end
