//
//  loginViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 2/24/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UITableViewController <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *linkedinWebView;

@property(retain, readwrite) NSString *accessToken;
@property (retain, readwrite) NSString *uuid;
@property (retain, readwrite) NSString *returnUser;
@property (retain, readwrite) NSString *expireDate;

@end
