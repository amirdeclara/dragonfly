//
//  SessionManager.m
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "SessionManager.h"
//#import "HTTPClient.h"
#import "Profile.h"


@implementation SessionManager


+ (SessionManager *) shared
{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}


- (void) promptLogin
{

}

- (void) sessionStarted
{
    LOG(@"session started");
    
    //self.loggedIn = YES;
    if(self.firstRun == YES)
    {
        self.firstRun = NO;
        
    }
    else
    {
        
    }
    
}

- (void) startSession
{
    //Make the call to check if the session already exists. If not invoke the Declara login screen
    
    [Profile profileWithCompletion:^(id result, NSError *error) {
        
        if(error) {
            
            return;
        }
        LOG(@"Success");
        self.loggedIn = YES;
        
    }];

}

- (void) endSession
{
    
}



@end
