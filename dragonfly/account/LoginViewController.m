//
//  loginViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 2/24/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "LoginViewController.h"
#import "SessionManager.h"
#import "Profile.h"
#import "MainNavigationViewController.h"
#import "HTTPSharedClient.h"
#import "Recommend.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self allowUserToLogin];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UIWebviewDelegate

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSURL *url = request.URL;
    NSString *urlString = url.absoluteString;
    
    BOOL userAllowedAccess = ([urlString rangeOfString:@"access_token"].location != NSNotFound);
    if ( userAllowedAccess )
    {
        [self setVerifierWithUrl:url];
        
        [self userProfile];
        
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.linkedinWebView sizeToFit];
    [self.linkedinWebView setFrame:CGRectMake(self.linkedinWebView.frame.origin.x, self.linkedinWebView.frame.origin.y, 400.0, 600)];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}

#pragma mark - UIViewController methods

-(void) userProfile {
    
   // NSString *user = [NSString stringWithFormat:@"%@/users/%@/profile", API_BASE_PATH, self.uuid ];
    LOG(@"The access token is %@", [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY]);
//    [Profile loadProfile:user accessToken: [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY] completion:^(id result, NSError *error)
//     {
//         
//         if(error) {
//            
//             return;
//         }
//         LOG(@"Success");
//         MainNavigationViewController *vc = [[MainNavigationViewController alloc] init];
//         [self presentViewController:vc animated:YES completion:nil];
//     }];
    
    [Profile profileWithCompletion:^(id result, NSError *error) {
        
        if(error) {
            
            return;
        }
        LOG(@"Success");
        
        //User profile succeeded, lets invoke the Main view of Declara App.
        MainNavigationViewController *vc = [[MainNavigationViewController alloc] init];
        [self presentViewController:vc animated:YES completion:nil];
        
    }];

    
}

- (void)allowUserToLogin
{
    //Send the request for login
    NSString *linkedinUrl = [NSString stringWithFormat:@"%@/%@", API_BASE_PATH, API_LOGIN_PATH];
    
    NSURL *url = [NSURL URLWithString:linkedinUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         //Invoke the web view of Declara login screen
         if ([data length] > 0 && error == nil) [self.linkedinWebView loadRequest:request];
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
}


- (void)setVerifierWithUrl:(NSURL *)aURL
{
    NSString *query = [aURL query];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs){
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        LOG(@"The elements is %@", elements);
        
        if ([[elements objectAtIndex:0] isEqualToString:@"?access_token"])
        {
            self.accessToken = [elements objectAtIndex:1];
            LOG(@"The self.verifier is %@", self.accessToken);
        }
        if(([[elements objectAtIndex:0] isEqualToString:@"uuid"])){
            
            self.uuid = [elements objectAtIndex:1];
            LOG(@"The self.uuid is %@", self.uuid);
        }
        if(([[elements objectAtIndex:0] isEqualToString:@"is_returning_user"])){
            
            self.returnUser = [elements objectAtIndex:1];
            LOG(@"The self.returnUser is %@", self.returnUser);
        }
        if(([[elements objectAtIndex:0] isEqualToString:@"expired_at"])){
            
            self.expireDate = [elements objectAtIndex:1];
            LOG(@"The self.expireDate is %@", self.expireDate);
        }
    }
    //}
    
    //Save the UUID and access token to NSUserDefault.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    [defaults setObject:self.accessToken forKey:ACCESS_TOKEN_KEY];
    //    [defaults setObject:[self base64String:self.accessToken] forKey:ACCESS_TOKEN_KEY];
    [defaults setObject:self.uuid forKey:UUID_TOKEN_KEY];
    [defaults synchronize];
    
    //Convert the access token to base64 encoding
    [HTTPSharedClient sharedClient].accessToken = [self base64String:self.accessToken];
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.shareActionGroup"];
    [shared setObject:self.uuid forKey:@"USER_ID"];
    [shared setObject:[self base64String:self.accessToken] forKey:@"ACCESS_TOKEN"];
    [shared synchronize];
    LOG(@"The ACCESS_TOKEN is %@", [shared valueForKey:@"ACCESS_TOKEN"]);
}

- (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

@end
