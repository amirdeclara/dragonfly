//
//  SessionManager.h
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionManager : NSObject


@property (nonatomic) BOOL resumingSession;
@property (nonatomic) BOOL refreshSession;
@property (nonatomic) BOOL loggedIn;
@property (nonatomic) BOOL firstRun;

+ (SessionManager *) shared;

- (void) startSession;
- (void) endSession;
- (void) sessionStarted;


@end
