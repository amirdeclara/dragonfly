//
//  AnotherUserNameHeaderTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "AnotherUserNameHeaderTableViewCell.h"

@implementation AnotherUserNameHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadProfileName : (NSString *) profileName {
    self.nameLabel.text = profileName;
    
}

@end
