//
//  AnotherUserFollowTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"

@protocol AnotherUserFollowTableViewCellDelegate <NSObject>

-(void) followCollection : (NSString *) collectionId;
-(void) unFollowCollection : (NSString *) collectionId;

@end


@interface AnotherUserFollowTableViewCell : UITableViewCell {
    
    NSString * booleanString;
    UserProfile *user;
    NSString *followString;
     NSString *userId;
}

@property (nonatomic, strong) IBOutlet UILabel *followLabel;

@property (nonatomic, strong) IBOutlet UIButton *followButton;

@property (nonatomic, weak) id<AnotherUserFollowTableViewCellDelegate> delegate;

-(void) loadFollowData : (UserProfile *) item;

-(IBAction)follow:(id)sender;

@end
