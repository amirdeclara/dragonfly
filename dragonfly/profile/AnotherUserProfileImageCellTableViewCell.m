//
//  AnotherUserProfileImageCellTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "AnotherUserProfileImageCellTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+declara.h"

@implementation AnotherUserProfileImageCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadProfileImage : (NSString *) imagePath {
    
    if (imagePath) {
        
        [self.profileImage sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"me"]];
        
        UIImage *imageCorner = [[UIImage alloc] init];
        [imageCorner circularImage: self.profileImage radius:25];
    } else {
        self.profileImage.image = nil;
    }
}

@end
