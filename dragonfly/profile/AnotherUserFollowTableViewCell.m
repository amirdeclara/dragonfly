//
//  AnotherUserFollowTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "AnotherUserFollowTableViewCell.h"

@implementation AnotherUserFollowTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadFollowData : (UserProfile *) item {
    
    userId = item.userId;
    followString = item.followed;

    if([followString isEqualToString:FOLLOW_VALUE]) {
        //toggleIsOn = YES;
        [self.followButton setTitle: @"unFollow" forState:UIControlStateNormal];
    }
    else{
        //toggleIsOn = NO;
        [self.followButton setTitle: @"Follow" forState:UIControlStateNormal];
    }
    
}









-(IBAction)follow:(id)sender {
    
    //NSString * booleanString = (self.item.followed) ? @"true" : @"false";
//    if([booleanString isEqualToString:@"true"]) {
//        [self.delegate unFollowCollection: user.userId];
//        [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
//    }
//    else{
//        [self.delegate followCollection:user.userId];
//        [self.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
//    }
    
    if([followString isEqualToString:@"1"]) {
        [self.delegate unFollowCollection: userId];
        [self.followButton setTitle: @"Follow" forState:UIControlStateNormal];
    }
    else{
        [self.delegate followCollection: userId];
        [self.followButton setTitle: @"UnFollow" forState:UIControlStateNormal];
    }
    
}

@end
