//
//  ProfileCollectionTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ProfileCollectionTableViewCell.h"
#import "UIImage+declara.h"

@implementation ProfileCollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) loadProfileCollectionData : (NSDictionary *) recommendCollectionData {
    LOG(@"The recommendCollectionData is %lu", (unsigned long)recommendCollectionData.count);
    
    self.collectionTitle.text = recommendCollectionData[@"title"];
    
    self.userFollowerLabel.text = [NSString stringWithFormat:@"%@ | %@ followers", recommendCollectionData[@"_embedded"][@"owners"][0][@"name"][@"full"], recommendCollectionData[@"_embedded"][@"followers"][@"count"]];
    
    NSString *imageUrl = recommendCollectionData[@"image_url"];
    if (imageUrl) {
        
        [self.collectionImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.collectionImage.image = nil;
    }
    
    UIImage *imageCorner = [[UIImage alloc] init];
    [imageCorner roundCornerView:self.collectionImage];
    
    self.collectionId = recommendCollectionData[@"id"];
    
    self.listOfCollectionItem.text = [NSString stringWithFormat:@"SEE ALL %@ ITEMS", recommendCollectionData[@"_embedded"][@"posts"][@"count"]];
}

@end
