//
//  AnotherUserNameHeaderTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnotherUserNameHeaderTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;

-(void) loadProfileName : (NSString *) profileName;

@end
