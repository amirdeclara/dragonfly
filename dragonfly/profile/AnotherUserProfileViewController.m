//
//  AnotherUserProfileViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "AnotherUserProfileViewController.h"
#import "ProgressHUD.h"
#import "UserProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+declara.h"
#import "DifferentUserCollection.h"
#import "ProfileCollectionTableViewCell.h"
#import "AnotherUserProfileImageCellTableViewCell.h"
#import "AnotherUserNameHeaderTableViewCell.h"
#import "AnotherUserFollowTableViewCell.h"
#import "CollectionArticleTableViewCell.h"
#import "HTTPSharedClient.h"

@interface AnotherUserProfileViewController () {
    UserProfile *item;
}

@end

@implementation AnotherUserProfileViewController

#define CELL_HEIGHT_HEADER 80
#define CELL_HEIGHT_FOLLOW 105
#define CELL_HEIGHT_COLLECTION 320
#define CLOSE_VIEW_ALPHA 0.25

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.collection = [[NSMutableArray alloc] init];
    [self loadUserProfile];
    //    [self loadCollectionData];
}

#pragma mark - UIViewController methods

-(void) loadUserProfile {
    
    [ProgressHUD show:@"Loading..."];
    
    NSString *profilePath = [NSString stringWithFormat:@"%@/users/%@/profile", API_BASE_PATH, self.userId ];
    
    [UserProfile userProfileWithCompletion: profilePath completion:^(id result, NSError *error) {
        
        [ProgressHUD dismiss];
        if(error) {
            
            return;
        }
        
        item = result;
        LOG(@"The item is %@", item);
        [self.tableView reloadData];
        [self loadCollectionData];
        
    }];
    
    
}

-(void) loadCollectionData{
    
    NSString *collectionPath = [NSString stringWithFormat:@"%@/users/%@/collections", API_BASE_PATH, self.userId];
    [DifferentUserCollection DifferentUserCollectionWithCompletion: collectionPath completion:^(id result, NSError *error) {
        
        [ProgressHUD dismiss];
        if(error) {
            
            return;
        }
        
        self.collection = result;
        LOG(@"The self.collection is %@", self.collection);
        [self.tableView reloadData];
        
    }];
}

#pragma mark - UITableViewDataSource

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    }
    else if (section == 1){
        return 1;
    }
    else{
        return self.collection.count;
    }
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        return CELL_HEIGHT_FOLLOW;
    }
    else if(indexPath.section == 1) {
        
        return CELL_HEIGHT_HEADER;
    }
    else{
        return CELL_HEIGHT_COLLECTION;
    }
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        AnotherUserProfileImageCellTableViewCell *imageCell = [tableView dequeueReusableCellWithIdentifier:@"AnotherUserProfileImageCellTableViewCell"];
        imageCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [imageCell loadProfileImage:item.imageUrl];
        return imageCell;
    }
    else if(indexPath.section == 1){
        AnotherUserFollowTableViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:@"AnotherUserFollowTableViewCell"];
        nameCell.selectionStyle = UITableViewCellSelectionStyleNone;
        nameCell.delegate = self;
        [nameCell loadFollowData:item];
        return nameCell;
        
    }
    else{
        ProfileCollectionTableViewCell *collectionCell;
        collectionCell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCollectionTableViewCell"];
        collectionCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [collectionCell loadProfileCollectionData:[self.collection objectAtIndex:indexPath.row]];
        return collectionCell;
    }
    
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 1) {
        AnotherUserNameHeaderTableViewCell *nameCell = [tableView dequeueReusableCellWithIdentifier:@"AnotherUserNameHeaderTableViewCell"];
        nameCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [nameCell loadProfileName:item.fullName];
        return nameCell;
    }
    else if(section == 2) {
        CollectionArticleTableViewCell *collectionCell = [tableView dequeueReusableCellWithIdentifier:@"CollectionArticleTableViewCell"];
        collectionCell.selectionStyle = UITableViewCellSelectionStyleNone;
        //[collectionCell loadProfileName:item.fullName];
        return collectionCell;
    }
    
    return nil;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1){
        return 50;
    }
    else if(section == 2) {
        return 40;
    }
    return 0;
}

- (BOOL)allowsHeaderViewsToFloat{
    return NO;
}


#pragma mark - IBAction

-(IBAction)closeCollection:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Collection view scrolling

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView.contentOffset.y > 0)
    {
        self.closeButton.imageView.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * scrollView.contentOffset.y);
        
        if(self.closeButton.imageView.alpha <= CLOSE_VIEW_ALPHA){
            self.closeButton.imageView.alpha = CLOSE_VIEW_ALPHA;
        }
        
    }
    else{
        self.closeButton.alpha = 1.0;
        
    }
    
}

#pragma mark - delegate

-(void) followCollection : (NSString *) collectionId{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following", API_BASE_PATH, uuid];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:collectionId forKey:@"id"];
    [parent setObject:@"user" forKey:@"type"];
    
    [[HTTPSharedClient sharedClient] POST:followPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

-(void) unFollowCollection : (NSString *) collectionId{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *followPath = [NSString stringWithFormat:@"%@/users/%@/following/%@", API_BASE_PATH, uuid, collectionId];
    
    [[HTTPSharedClient sharedClient] DELETE:followPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.tableView reloadData];
        LOG(@"Success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}


@end
