//
//  AnotherUserProfileViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnotherUserFollowTableViewCell.h"

@interface AnotherUserProfileViewController : UIViewController <AnotherUserFollowTableViewCellDelegate>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *collectionId;
@property (nonatomic, strong) NSMutableArray *collection;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIImageView *articlesLine;
@property (nonatomic, strong) IBOutlet UIImageView *collectionsLine;
@property (nonatomic, strong) IBOutlet UIButton *articleBtn;
@property (nonatomic, strong) IBOutlet UIButton *collectionBtn;
@property (nonatomic, strong) IBOutlet UIView *closeView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;


-(IBAction)closeCollection:(id)sender;

@end
