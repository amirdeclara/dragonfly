//
//  ProfileCollectionTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol ProfileCollectionTableViewCellDelegate <NSObject>

-(void) followCollection : (NSString *) collectionId;
-(void) unFollowCollection : (NSString *) collectionId;

@end

@interface ProfileCollectionTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *collectionImage;
@property (nonatomic, strong) IBOutlet UILabel *collectionTitle;
@property (nonatomic, strong) IBOutlet UILabel *userFollowerLabel;
@property (nonatomic, strong) IBOutlet UILabel *itemCollectTitle;
@property (nonatomic, strong) IBOutlet UILabel *noOfItemCollect;
@property (nonatomic, strong) IBOutlet UILabel *listOfCollectionItem;

//@property (nonatomic, strong) IBOutlet UIButton *followBtn;

@property (nonatomic, strong) NSString *collectionId;


@property (nonatomic, weak) id<ProfileCollectionTableViewCellDelegate> delegate;


-(void) loadProfileCollectionData : (NSDictionary *) recommendCollectionData;


//-(IBAction)follow:(id)sender;

//-(IBAction)loadCollections:(id)sender;


@end
