//
//  AnotherUserProfileImageCellTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnotherUserProfileImageCellTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *profileImage;

-(void) loadProfileImage : (NSString *) imagePath;

@end
