//
//  UIFont+declara.h
//  dragonfly
//
//  Created by Amirul Islam on 3/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (declara)

+ (UIFont *)defaultFont:(NSInteger) size;

+ (UIFont *)defaultFontBold:(NSInteger) size;

+ (UIFont *)defaultFontRegular:(NSInteger) size;

@end
