//
//  NSString+declara.m
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "NSString+declara.h"

@implementation NSString (declara)


+ (NSString *)stringByStrippingHTML:(NSString *)inputString
{
    NSMutableString *outString;
    
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        
        if ([inputString length] > 0)
        {
            NSRange r;
            
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    
    return outString;
}


+(NSString *) getDomainName :(NSString *) inputString
{
    NSURL *url = [NSURL URLWithString:inputString];
    return [url host];
    
}
@end
