//
//  UIFont+declara.m
//  dragonfly
//
//  Created by Amirul Islam on 3/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UIFont+declara.h"

@implementation UIFont (declara)

+ (UIFont *)defaultFont:(NSInteger) size
{
    
    return [UIFont fontWithName:@"PT Serif" size:size];
}

+ (UIFont *)defaultFontBold:(NSInteger) size {
    
    return [UIFont fontWithName:@"Montserrat-Bold" size:size];
    
}

+ (UIFont *)defaultFontRegular:(NSInteger) size {
    
    return [UIFont fontWithName:@"Montserrat-Regular" size:size];
    
}

@end
