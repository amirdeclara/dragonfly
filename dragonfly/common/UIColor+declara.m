//
//  UIColor+declara.m
//  dragonfly
//
//  Created by Amirul Islam on 3/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UIColor+declara.h"

@implementation UIColor (declara)

+ (UIColor *)colorRed {
    static UIColor *color;
    if ( color == nil ) {
        color = [UIColor colorWithRed:255.0f/255.0f green:65.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
    }
    
    return color;
}

@end
