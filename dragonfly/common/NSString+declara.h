//
//  NSString+declara.h
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (declara)

+ (NSString *)stringByStrippingHTML:(NSString *)inputString;

+(NSString *) getDomainName :(NSString *) inputString;

@end
