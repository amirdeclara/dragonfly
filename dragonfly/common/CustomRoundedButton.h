//
//  CustomRoundedButton.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomRoundedButton : UIButton

@property (nonatomic, assign) int style;

@end
