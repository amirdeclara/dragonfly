//
//  UITextView+declara.h
//  dragonfly
//
//  Created by Amirul Islam on 3/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (declara)

- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;


@end
