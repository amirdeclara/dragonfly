//
//  CustomHeaderCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/9/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomHeaderCellDelegate <NSObject>

- (void)shareArticle;

- (void)collectArticle;

- (void)likeArticle;

@end

@interface CustomHeaderCell : UITableViewCell

@property (nonatomic, weak) id<CustomHeaderCellDelegate> delegate;

- (IBAction)shareButtonTapped:(id)sender;

- (IBAction)collectButtonTapped:(id)sender;

- (IBAction)likeButtonTapped:(id)sender;

@end
