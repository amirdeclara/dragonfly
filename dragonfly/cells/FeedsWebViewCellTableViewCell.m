//
//  FeedsWebViewCellTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/9/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedsWebViewCellTableViewCell.h"


static CGFloat kPaddingDist = 8.0f;
static CGFloat kDefaultCommentCellHeight = 40.0f;
static CGFloat kTableViewWidth = -1;
static CGFloat kStandardButtonSize = 50.0f;
static CGFloat kStandardLabelHeight = 20.0f;

#define kCommentCellFont  [UIFont fontWithName:@"Helvetica" size:15]

@implementation FeedsWebViewCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
    NSString *comment = self.comment;
    CGFloat cellHeight = [FeedsWebViewCellTableViewCell heightForComment:comment];
    CGRect frame = self.contentBody.frame;
    frame.size.height = cellHeight;
    self.contentBody.frame = frame;
    
    [super layoutSubviews];
}

#pragma mark -
#pragma mark Interface

+ (void)setTableViewWidth:(CGFloat)tableWidth
{
    kTableViewWidth = tableWidth;
}

+ (id)storyCommentCellForTableWidth:(CGFloat)width
{
    FeedsWebViewCellTableViewCell *cell = [[FeedsWebViewCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FeedsWebViewCellTableViewCell"];
    
    CGRect cellFrame = cell.frame;
    cellFrame.size.width = width;
    cell.frame = cellFrame;
    
    
    
    //Comment Label
    CGRect labelRect = CGRectMake(20,
                                  0,
                                  cell.frame.size.width-40,
                                  kStandardLabelHeight);
    UILabel *commentlabe = [[UILabel alloc] initWithFrame:labelRect];
    commentlabe.font = kCommentCellFont;
    commentlabe.textColor = [UIColor darkGrayColor];
    commentlabe.textAlignment = NSTextAlignmentLeft;
    commentlabe.numberOfLines = 0;
    commentlabe.lineBreakMode = NSLineBreakByWordWrapping;
    commentlabe.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    cell.contentBody = commentlabe;
    [cell addSubview:commentlabe];
    
    
    return cell;
}

+ (CGFloat)cellHeightForComment:(NSString *)comment
{
    NSLog(@"The comment Amir is is %@", comment);
    return kDefaultCommentCellHeight + [FeedsWebViewCellTableViewCell heightForComment:comment];
}

+ (CGFloat)heightForComment:(NSString *)comment
{
    NSLog(@"The comment Amir is is %@", comment);
    
    CGFloat height = 0.0;
    CGFloat commentlabelWidth = kTableViewWidth - 2 * (kStandardButtonSize + kPaddingDist);
    CGRect rect = [comment boundingRectWithSize:(CGSize){commentlabelWidth, MAXFLOAT}
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:kCommentCellFont}
                                        context:nil];
    height = rect.size.height;
    NSLog(@"The height is %f", height);
    return height;
}

- (void)configureCommentCellForComment:(NSString *)comment
{
    
    self.comment = comment;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[comment dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    self.contentBody.attributedText = attrStr;
    self.contentBody.text = comment;
    
    [self setNeedsLayout];
}

@end
