//
//  RecommendCollectionTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "RecommendCollectionTableViewCell.h"
#import "UIImage+declara.h"
#import "UIImage+BlurredFrame.h"

@implementation RecommendCollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadRecommendCollectionData : (NSDictionary *) recommendCollectionData {
    LOG(@"The recommendCollectionData is %@", recommendCollectionData);
    
    self.collectionTitle.text = recommendCollectionData[@"_embedded"][@"item"][@"title"];
    
    NSString *user = recommendCollectionData[@"_embedded"][@"item"][@"_embedded"][@"owners"][0][@"name"][@"full"];
    NSString *follow = recommendCollectionData[@"_embedded"][@"item"][@"_embedded"][@"owners"][0][@"total_followers_count"];
    
    self.userName.text = [NSString stringWithFormat:@"%@ | %@ followers", user, follow];
    
    NSString *imageUrl = recommendCollectionData[@"_embedded"][@"item"][@"image_url"];
    if (imageUrl) {
        
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.userImage.image = nil;
    }
    
    UIImage *imageCorner = [[UIImage alloc] init];
    [imageCorner roundCornerView:self.userImage];
    
    self.collectionId = recommendCollectionData[@"_embedded"][@"item"][@"id"];
    followString = [recommendCollectionData[@"_embedded"][@"item"][@"followed"] stringValue];
    
    if([followString isEqualToString:FOLLOW_VALUE]) {
        [self.followBtn setTitle: @"unFollow" forState:UIControlStateNormal];
    }
    else{
        [self.followBtn setTitle: @"Follow" forState:UIControlStateNormal];
    }
    
    self.listOfCollectionItem.text = [NSString stringWithFormat:@"SEE ALL %@ ITEMS", recommendCollectionData[@"_embedded"][@"item"][@"_embedded"][@"posts"][@"count"]];
}


-(IBAction)follow:(id)sender {
    if([followString isEqualToString:FOLLOW_VALUE]) {
        [self.delegate unFollowCollection: self.collectionId];
        [self.followBtn setTitle: @"Follow" forState:UIControlStateNormal];
    }
    else{
        [self.delegate followCollection: self.collectionId];
        [self.followBtn setTitle: @"UnFollow" forState:UIControlStateNormal];
    }
}

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view
{
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    
    float distanceFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.userImage.frame) - CGRectGetHeight(self.frame);
    float move = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference;
    
    CGRect imageRect = self.userImage.frame;
    imageRect.origin.y = -(difference/2)+move;
    self.userImage.frame = imageRect;
}

@end
