//
//  UserCollectionTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface UserCollectionTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic, weak) IBOutlet UIImageView *collectionImage;
@property (nonatomic, weak) IBOutlet UIImageView *likeImage;
@property (nonatomic, strong) IBOutlet UILabel *collectionTitle;
@property (nonatomic, strong) IBOutlet UILabel *daysLeft;
@property (nonatomic, weak) IBOutlet UILabel *likeLabel;
@property (nonatomic, weak) IBOutlet UILabel *collectionCountLabel;
@property (nonatomic, weak) NSString *collectionId;
@property (nonatomic, weak) IBOutlet UIImageView *favImage;

-(void) loadUserCollectionData : (NSDictionary *) userCollectionData userImage : (NSString *) imageUrl;

@end
