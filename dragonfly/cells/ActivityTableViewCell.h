//
//  ActivityTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;

@property (nonatomic, strong) IBOutlet UILabel *contentTitle;

@property (nonatomic, strong) IBOutlet UILabel *daysLeft;

-(void) loadNotification : (NSDictionary *) feedData;

@end
