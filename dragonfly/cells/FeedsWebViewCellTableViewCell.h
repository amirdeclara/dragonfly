//
//  FeedsWebViewCellTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/9/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedsWebViewCellTableViewCellDelegate <NSObject>

-(void) changeWebViewHeight:(NSInteger) h;

@end

@interface FeedsWebViewCellTableViewCell : UITableViewCell

@property (nonatomic, weak) id<FeedsWebViewCellTableViewCellDelegate> delegate;

@property(nonatomic) BOOL suppressesIncrementalRendering;

@property (nonatomic, strong) IBOutlet UILabel *contentBody;
@property (nonatomic) NSString *comment;

+ (void)setTableViewWidth:(CGFloat)tableWidth;
+ (id)storyCommentCellForTableWidth:(CGFloat)width;
+ (CGFloat)cellHeightForComment:(NSString *)comment;
- (void)configureCommentCellForComment:(NSString *)comment;

@end
