//
//  UserCollectionTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UserCollectionTableViewCell.h"
#import "UIImage+declara.h"
#import <NSDate+TimeAgo/NSDate+TimeAgo.h>

@implementation UserCollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadUserCollectionData : (NSDictionary *) userCollectionData userImage : (NSString *) userImageUrl{
    LOG(@"The userCollectionData is %@", userCollectionData);
    
    self.collectionId = userCollectionData[@"id"];
    self.collectionTitle.text = userCollectionData[@"_embedded"][@"content"][@"title"];
    
    //BOOL like = userCollectionData[@"_embedded"][@"content"][@"liked"];
    // self.likeLabel.text = [userCollectionData[@"_embedded"][@"content"][@"_embedded"] [@"likes"][@"count"] stringValue];
    
    self.collectionCountLabel.text = [userCollectionData[@"_embedded"][@"content"][@"_embedded"] [@"collections"][@"count"] stringValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *dtPostDate = [dateFormatter dateFromString:userCollectionData[@"created_time"]];
    self.daysLeft.text = [NSString stringWithFormat:@"%@ ago", [dtPostDate timeAgoSimple]];
    
    UIImage *imageCorner = [[UIImage alloc] init];
    NSString *imageUrl = userCollectionData[@"_embedded"][@"content"][@"image_url"];
    if (imageUrl) {
        [self.collectionImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.collectionImage.image = [UIImage imageNamed:@"greysquarepattern.png"];
    }
    [imageCorner roundCornerView:self.collectionImage];
    
    if (userImageUrl) {
        
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:userImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.userImage.image = nil;
    }
    [imageCorner circularImage: self.userImage radius:22.5];
    
    NSString *favImageUrl = userCollectionData[@"_embedded"][@"content"][@"favicon"];
    
    if (favImageUrl) {
        
        [self.favImage sd_setImageWithURL:[NSURL URLWithString:favImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.favImage.image = nil;
    }
    
}



@end
