//
//  feedsTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedsTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleDescription;
@property (nonatomic, weak) IBOutlet UIImageView *feedImage;
@property (nonatomic, strong) IBOutlet UILabel *authorName;

-(void) loadData : (NSDictionary *) feedData;


@end
