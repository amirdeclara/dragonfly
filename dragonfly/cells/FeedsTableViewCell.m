//
//  feedsTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedsTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation FeedsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadData : (NSDictionary *) feedData {
    LOG(@"The feedData is %@", feedData);
    
    self.titleDescription.text = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"description"];
    self.authorName.text = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"author_name"];
    NSString *imageUrl = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"image_url"];
    if (imageUrl) {
        [self.feedImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.feedImage.image = nil;
    }
}

@end
