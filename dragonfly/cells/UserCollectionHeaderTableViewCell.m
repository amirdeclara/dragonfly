//
//  UserCollectionHeaderTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UserCollectionHeaderTableViewCell.h"

@implementation UserCollectionHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadCollectionData : (UserCollection *) item {
    
    NSMutableArray *ownerArray = [[NSMutableArray alloc] init];
    NSString *ownerStr = @"";
    NSInteger num;
    for (int i=0; i<item.owners.count; i++){
        NSString *own = [item.owners objectAtIndex:i][@"name"][@"full"];
        LOG(@"The own is %@", own);
        if(i < 2){
            [ownerArray addObject:own];
        }
    }
    ownerStr = [ownerArray componentsJoinedByString:@","];
    
    if(item.owners.count>2){
        num = item.owners.count - 2;
        NSString *numFollow = [NSString stringWithFormat:@"%ld", (long)num];
        self.followerName.text = [NSString stringWithFormat:@"%@ + %@", ownerStr, numFollow];
    }
    else{
        self.followerName.text = [NSString stringWithFormat:@"%@", ownerStr];
    }
    
    collectionId = item.collectionId;
    
    followString = item.followed;
    
    if([followString isEqualToString:FOLLOW_VALUE]) {
        [self.followButton setTitle: @"unFollow" forState:UIControlStateNormal];
    }
    else{
        [self.followButton setTitle: @"Follow" forState:UIControlStateNormal];
    }
}


-(IBAction)follow:(id)sender {
    
    if([followString isEqualToString:FOLLOW_VALUE]) {
        [self.delegate unFollowCollection: collectionId];
        [self.followButton setTitle: @"Follow" forState:UIControlStateNormal];
    }
    else{
        [self.delegate followCollection: collectionId];
        [self.followButton setTitle: @"UnFollow" forState:UIControlStateNormal];
    }
    
}

@end
