//
//  RecommendationTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "RecommendationTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation RecommendationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadRecommendData : (NSDictionary *) recommendData {
    LOG(@"The feedData is %@", recommendData);
    
    self.userName.text = recommendData[@"_embedded"][@"item"][@"name"][@"full"];
    NSString *imageUrl = recommendData[@"_embedded"][@"item"][@"image_url"];
    if (imageUrl) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.userImage.image = nil;
    }
}

@end
