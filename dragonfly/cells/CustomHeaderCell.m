//
//  CustomHeaderCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/9/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "CustomHeaderCell.h"

@implementation CustomHeaderCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)shareButtonTapped:(id)sender {
    [self.delegate shareArticle];

}


- (IBAction)collectButtonTapped:(id)sender {
    [self.delegate collectArticle];
 
}



- (IBAction)likeButtonTapped:(id)sender {
    [self.delegate likeArticle];
}

@end
