//
//  RecommendationTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendationTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;

-(void) loadRecommendData : (NSDictionary *) recommendData;

@end
