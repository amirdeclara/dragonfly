//
//  RecommendPeopleTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "RecommendPeopleTableViewCell.h"
#import "UIImage+declara.h"

@implementation RecommendPeopleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadRecommendPeopleData : (NSDictionary *) recommendPeopleData {
    LOG(@"The recommendPeopleData is %@", recommendPeopleData);
    
    self.userName.text = recommendPeopleData[@"_embedded"][@"item"][@"name"][@"full"];
    NSString *imageUrl = recommendPeopleData[@"_embedded"][@"item"][@"image_url"];
    if (imageUrl) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.userImage.image = nil;
    }
    UIImage *imageCorner = [[UIImage alloc] init];
    [imageCorner circularImage: self.userImage radius:23];
    
    NSString *follower = recommendPeopleData[@"_embedded"][@"item"][@"total_followers_count"];
    NSString *collections = recommendPeopleData[@"_embedded"][@"item"][@"total_collections_followed"];
    self.followersLabel.text = [NSString stringWithFormat:@"%@ followers | %@ collections", follower, collections ];
    self.userId = recommendPeopleData[@"_embedded"][@"item"][@"id"];
    
}


-(IBAction)loadUserProfile:(id)sender {
    [self.delegate loadUserProfile : self.userId];
}
@end
