//
//  CollectionTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "CollectionTableViewCell.h"

@implementation CollectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadCollectData : (NSDictionary *) collectionData {
    LOG(@"The recommendCollectionData is %@", collectionData);
     self.collectionTitle = collectionData[@"title"];
    self.collectionName.text = collectionData[@"title"];
    //[self.collectionButton setTitle:self.collectionTitle forState:UIControlStateNormal];
}


-(IBAction)collect:(id)sender {
    [self.delegate addCollection: self.collectionTitle];
}


@end
