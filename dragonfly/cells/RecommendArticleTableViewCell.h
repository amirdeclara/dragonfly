//
//  RecommendArticleTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol RecommendArticleTableViewCellDelegate <NSObject>

-(void) loadUserCollection : (NSString *) usedId collectionId : (NSString *) collectionId;

@end

@interface RecommendArticleTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic, weak) IBOutlet UIImageView *contentImage;
@property (nonatomic, strong) IBOutlet UILabel *contentTitle;
@property (nonatomic, strong) IBOutlet UILabel *daysLeft;
@property (nonatomic, strong) IBOutlet UIView *cellView;
@property (nonatomic, strong) IBOutlet UILabel *collectionLabel;
@property (nonatomic, strong) IBOutlet UIImageView *collectionImage;
@property (nonatomic, weak) IBOutlet UIButton *collectionBtn;
@property (nonatomic, weak) NSString *collectionId;
@property (nonatomic, weak) NSString *userId;
@property (nonatomic, weak) id<RecommendArticleTableViewCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIImageView *favImage;
@property (nonatomic, strong) IBOutlet UILabel *collectionName;

-(void) loadRecommendArticleData : (NSDictionary *) recommendArticleData;
- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;
-(IBAction)loadCollection:(id)sender;

@end
