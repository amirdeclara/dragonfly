//
//  FeedReadingListViewTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/17/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedReadingListViewTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *detailLabel;
@end
