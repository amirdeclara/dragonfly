//
//  CollectionTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol AddCollectionTableViewCellDelegate <NSObject>

-(void) addCollection : (NSString *) collectionId;

@end

@interface CollectionTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *collectionTitle;
@property (nonatomic, strong)IBOutlet UIButton *collectionButton;
@property (nonatomic, weak) id<AddCollectionTableViewCellDelegate> delegate;
@property (nonatomic, strong) IBOutlet UILabel *collectionName;

-(void) loadCollectData : (NSDictionary *) collectionData;

@end
