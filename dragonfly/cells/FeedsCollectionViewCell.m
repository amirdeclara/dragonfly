//
//  FeedsCollectionViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedsCollectionViewCell.h"
#import "NSDate+TimeAgo.h"
#import "UIImage+declara.h"
#import <NSDate+TimeAgo/NSDate+TimeAgo.h>
#import "NSString+declara.h"
#import "UIFont+declara.h"

@implementation FeedsCollectionViewCell


-(void) loadData : (NSDictionary *) feedData {
    LOG(@"The feedData is %@", feedData);
    
    self.bodyDescription.text = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"description"];
    
    NSMutableDictionary *regularFont = [[NSMutableDictionary alloc] init];
    [regularFont setObject:[UIFont defaultFont:17] forKey:NSFontAttributeName];
    NSMutableAttributedString *header = [[NSMutableAttributedString alloc] initWithString:feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"description"] attributes:regularFont];
    [self.titleDescription setAttributedText:header];
    
    NSString *domain = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"source_url"];
    self.authorName.text = [NSString getDomainName:domain];
    self.title.text = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"title"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *dtPostDate = [dateFormatter dateFromString:feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"created_time"]];
    self.timeLeft.text = [NSString stringWithFormat:@"%@ ago", [dtPostDate timeAgoSimple]];
    self.collects.text = [NSString stringWithFormat:@"%@", feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"_embedded"][@"collections"][@"count"]];
    self.userId = feedData[@"_embedded"][@"item"][@"_embedded"][@"creator"][@"id"];
    LOG(@"the self.userId is %@", self.userId);
    
    NSString *imageUrl = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"image_url"];
    if (imageUrl) {
        [self.feedImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.feedImage.image = nil;
    }
    
    UIImage *imageCorner = [[UIImage alloc] init];
    [imageCorner roundCornerView:self.feedImage];
    
    NSString *userImageUrl = feedData[@"_embedded"][@"item"][@"_embedded"][@"creator"][@"image_url"];
    if (userImageUrl) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:userImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        [imageCorner circularImage:self.userImage radius:22.5];
        
    } else {
        self.userImage.image = nil;
    }
    
    NSString *favImageUrl = feedData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"favicon"];
    if (favImageUrl) {
        [self.favImage sd_setImageWithURL:[NSURL URLWithString:favImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
    } else {
        self.favImage.image = nil;
    }
    
    self.collectionName.text = feedData[@"_embedded"][@"item"][@"collection_title"];
    self.collectionId = feedData[@"_embedded"][@"item"][@"collection_id"];
}

- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    return 9;
}

-(IBAction)loadCollection:(id)sender {
    [self.delegate loadUserCollection: self.userId collectionId: self.collectionId];
}

@end
