//
//  FeedReadingListViewTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/17/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "FeedReadingListViewTableViewCell.h"

@implementation FeedReadingListViewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
