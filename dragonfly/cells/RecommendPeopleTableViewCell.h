//
//  RecommendPeopleTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol RecommendPeopleTableViewCellDelegate <NSObject>

-(void) loadUserProfile : (NSString *) usedId;

@end

@interface RecommendPeopleTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) IBOutlet UILabel *followersLabel;
@property (nonatomic, weak) id<RecommendPeopleTableViewCellDelegate> delegate;

-(void) loadRecommendPeopleData : (NSDictionary *) recommendPeopleData;
-(IBAction)loadUserProfile:(id)sender;

@end
