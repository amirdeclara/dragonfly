//
//  RecommendArticleTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "RecommendArticleTableViewCell.h"
#import "UIImage+declara.h"
#import <NSDate+TimeAgo/NSDate+TimeAgo.h>

@implementation RecommendArticleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadRecommendArticleData : (NSDictionary *) recommendArticleData {
    LOG(@"The feedData is %@", recommendArticleData);
    
    self.userName.text = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"creator"][@"name"][@"full"];
    self.contentTitle.text = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"title"];
    self.collectionLabel.text = [recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"_embedded"][@"collections"][@"count"] stringValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *dtPostDate = [dateFormatter dateFromString:recommendArticleData[@"_embedded"][@"item"][@"created_time"]];
    self.daysLeft.text = [NSString stringWithFormat:@"%@ ago", [dtPostDate timeAgoSimple]];
    self.collectionName.text = recommendArticleData[@"_embedded"][@"item"][@"collection_title"];
    
    self.collectionId = recommendArticleData[@"_embedded"][@"item"][@"collection_id"];
    self.userId = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"creator"][@"id"];\
    
    UIImage *imageCorner = [[UIImage alloc] init];
    NSString *imageUrl = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"image_url"];
    if (imageUrl) {
        [self.contentImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.contentImage.image = [UIImage imageNamed:@"greysquarepattern.png"];
    }
    [imageCorner roundCornerView:self.contentImage];
    
    NSString *userImageUrl = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"creator"][@"image_url"];
    NSLog(@"The userImageUrl value is Amir %@", userImageUrl);
    if (userImageUrl) {
        
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:userImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.userImage.image = nil;
    }
    [imageCorner circularImage: self.userImage radius:22.5];
    
    
    NSString *favImageUrl = recommendArticleData[@"_embedded"][@"item"][@"_embedded"][@"content"][@"favicon"];
    if (favImageUrl) {
        
        [self.favImage sd_setImageWithURL:[NSURL URLWithString:favImageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        
    } else {
        self.favImage.image = nil;
    }
    
}

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view
{
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    
    float distanceFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.contentImage.frame) - CGRectGetHeight(self.frame);
    float move = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference;
    
    CGRect imageRect = self.contentImage.frame;
    imageRect.origin.y = -(difference/2)+move;
    self.contentImage.frame = imageRect;
}


-(IBAction)loadCollection:(id)sender {
    
    [self.delegate loadUserCollection: self.userId collectionId: self.collectionId];
}

@end
