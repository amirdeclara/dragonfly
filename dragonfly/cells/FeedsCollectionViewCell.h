//
//  FeedsCollectionViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol FeedsCollectionViewCellDelegate <NSObject>
-(void) loadUserCollection : (NSString *) usedId collectionId : (NSString *) collectionId;
@end

@interface FeedsCollectionViewCell : UICollectionViewCell <UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *feedImage;
@property (nonatomic, strong) IBOutlet UILabel *authorName;
@property (nonatomic, strong) IBOutlet UILabel *timeLeft;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic, strong) IBOutlet UILabel *title;
@property (nonatomic, strong) IBOutlet UILabel *collects;
@property (nonatomic, weak) IBOutlet UIImageView *likeImage;
@property (nonatomic, weak) id<FeedsCollectionViewCellDelegate> delegate;
@property (nonatomic, strong) IBOutlet UITextView *titleDescription;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, weak) IBOutlet UIImageView *favImage;
@property (nonatomic, weak) IBOutlet UIButton *collectionBtn;
@property (nonatomic, weak) NSString *collectionId;
@property (nonatomic, strong) IBOutlet UILabel *bodyDescription;
@property (nonatomic, strong) IBOutlet UILabel *collectionName;

-(void) loadData : (NSDictionary *) feedData;
-(IBAction)loadCollection:(id)sender;

@end
