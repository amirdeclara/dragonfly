//
//  ActivityTableViewCell.m
//  dragonfly
//
//  Created by Amirul Islam on 2/26/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ActivityTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <NSDate+TimeAgo/NSDate+TimeAgo.h>
#import "UIImage+declara.h"
#import "UIFont+declara.h"

@implementation ActivityTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadNotification : (NSDictionary *) notificationData {
    
    NSString *imageUrl = @"";
    
    NSMutableDictionary *regularFont = [[NSMutableDictionary alloc] init];
    [regularFont setObject:[UIFont defaultFontRegular:11] forKey:NSFontAttributeName];
    NSMutableDictionary *boldFont = [[NSMutableDictionary alloc] init];
    [boldFont setObject:[UIFont defaultFontBold:12] forKey:NSFontAttributeName];
    NSMutableAttributedString *firstStr;
    NSMutableAttributedString *secondStr;
    
    if([notificationData[@"type"] isEqualToString:@"N_new_follower"]){
        self.userName.text = notificationData[@"data"][@"user_name"];
        imageUrl = notificationData[@"data"][@"user_image"];
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has started following you" attributes:regularFont];
        
    }
    else if([notificationData[@"type"] isEqualToString:@"N_new_linkedin_connection_joined"]){
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has joined Declara" attributes:regularFont];
    }
    else if([notificationData[@"type"] isEqualToString:@"N_new_collection_follower"]){
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"is now following your collection" attributes:regularFont];
    }
    else if([notificationData[@"type"] isEqualToString:@"collectionShare"]){
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has shared your collection" attributes:regularFont];
    }
    else if([notificationData[@"type"] isEqualToString:@"N_new_content_in_collection"]){
        self.userName.text = notificationData[@"data"][@"collection_owner_name"];
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has added a post to" attributes:regularFont];
        secondStr = [[NSMutableAttributedString alloc] initWithString:notificationData[@"data"][@"collection_title"] attributes:boldFont];
        imageUrl = notificationData[@"data"][@"collection_owner_image"];
    }
    else if([notificationData[@"type"] isEqualToString:@"N_new_collection"]){
        self.userName.text = notificationData[@"data"][@"collection_owner_name"];
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has added a collection" attributes:regularFont];
        secondStr = [[NSMutableAttributedString alloc] initWithString:notificationData[@"data"][@"collection_title"] attributes:boldFont];
        imageUrl = notificationData[@"data"][@"collection_owner_image"];
    }
    else if([notificationData[@"type"] isEqualToString:@"N_like_content"]){
        self.userName.text = notificationData[@"data"][@"user_name"];
        firstStr = [[NSMutableAttributedString alloc] initWithString:@"has liked the content" attributes:regularFont];
        LOG(@"The value Amir is %@", notificationData[@"data"][@"collection_title"]);
        NSString *str = notificationData[@"data"][@"collection_title"];
        if(str.length > 0){
            secondStr = [[NSMutableAttributedString alloc] initWithString:notificationData[@"data"][@"collection_title"] attributes:boldFont];
        }
        //[firstStr appendAttributedString:secondStr];
        imageUrl = notificationData[@"data"][@"user_image"];
    }

    if(secondStr.length > 0){
        [firstStr appendAttributedString:secondStr];
    }
    
    [self.contentTitle setAttributedText:firstStr];
    
    if (imageUrl) {
        
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"me"]];
        UIImage *imageCorner = [[UIImage alloc] init];
        [imageCorner circularImage: self.userImage radius:22.5];
    } else {
        self.userImage.image = nil;
    }
    
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:(int)notificationData[@"created"]];
    self.daysLeft.text = [NSString stringWithFormat:@"%@ ago", [date timeAgoSimple]];
    
}


@end
