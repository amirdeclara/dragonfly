//
//  RecommendCollectionTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/2/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol RecommendCollectionTableViewCellDelegate <NSObject>

-(void) followCollection : (NSString *) collectionId;
-(void) unFollowCollection : (NSString *) collectionId;

@end

@interface RecommendCollectionTableViewCell : UITableViewCell {
    
    BOOL toggleIsOn;
    NSString * followString;
}

@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UIImageView *userImage;
@property (nonatomic, strong) IBOutlet UILabel *collectionTitle;
@property (nonatomic, strong) IBOutlet UIButton *followBtn;
@property (nonatomic, weak) id<RecommendCollectionTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSString *collectionId;
@property (nonatomic, strong) IBOutlet UILabel *itemCollectTitle;
@property (nonatomic, strong) IBOutlet UILabel *noOfItemCollect;
@property (nonatomic, strong) IBOutlet UILabel *listOfCollectionItem;

-(void) loadRecommendCollectionData : (NSDictionary *) recommendCollectionData;
-(IBAction)follow:(id)sender;
- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;

@end
