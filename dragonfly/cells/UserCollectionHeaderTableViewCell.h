//
//  UserCollectionHeaderTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCollection.h"
#import "CustomRoundedButton.h"

@protocol UserCollectionHeaderTableViewCellDelegate <NSObject>

-(void) followCollection : (NSString *) collectionId;
-(void) unFollowCollection : (NSString *) collectionId;

@end



@interface UserCollectionHeaderTableViewCell : UITableViewCell {
    
    CustomRoundedButton *customButton;
    NSString *followString;
    NSString *collectionId;
}

@property(nonatomic, strong) IBOutlet UILabel *followerName;
@property(nonatomic, strong) IBOutlet UIButton *followButton;
@property (nonatomic, weak) id<UserCollectionHeaderTableViewCellDelegate> delegate;

-(void) loadCollectionData : (UserCollection *) item;
-(IBAction)follow:(id)sender;

@end
