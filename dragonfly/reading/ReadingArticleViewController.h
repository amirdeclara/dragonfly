//
//  ReadingArticleViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadingArticleViewController : UIViewController<UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *contentBodyView;
@property (nonatomic, strong) IBOutlet UIView *closeView;
@property (nonatomic, strong) IBOutlet UILabel *headerTitle;
@property (nonatomic, strong) IBOutlet UILabel *favTitle;
@property (nonatomic, strong) IBOutlet UILabel *contentBody;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) NSString *contentId;

@property (nonatomic, strong) IBOutlet UIButton *shareButton;
@property (nonatomic, strong) IBOutlet UIButton *collectButton;

-(IBAction)closeWebView:(id)sender;
-(IBAction)shareContent:(id)sender;
-(IBAction)collectContent:(id)sender;


@property (nonatomic, strong) UIPopoverController *popController;

@end
