//
//  ReadingArticleTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadingArticleTableViewCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UIView *contentBodyView;
@property (nonatomic, strong) IBOutlet UIView *closeView;
@property (nonatomic, strong) IBOutlet UILabel *headerTitle;
@property (nonatomic, strong) IBOutlet UILabel *favTitle;
@property (nonatomic, strong) IBOutlet UITextView *contentBody;

- (void)populate :(NSString *)body;

@end
