//
//  AddToCollectionViewController.h
//  dragonfly
//
//  Created by Amirul Islam on 3/22/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddToCollectionViewController : UIViewController <UITextFieldDelegate>


@property (nonatomic, strong) NSArray* collections;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *collectionsView;

@property (weak, nonatomic) IBOutlet UIButton *createCollectionButton;

@property (weak, nonatomic) IBOutlet UITableView *collectionsTableView;

@property (weak, nonatomic) IBOutlet UITextField *collectionsTextField;

@property (weak, nonatomic) IBOutlet UIButton *shadeButton;

@property (weak, nonatomic) IBOutlet UIView *collectionsInnerRoundedView;

@property (nonatomic, strong) NSString *collectionName;

@property (nonatomic, weak) NSString *contentUrl;

@property (nonatomic, strong) NSString *contentDescription;

-(IBAction)createCollection:(id)sender;

//-(IBAction)createCollectionButtonPressed:(id)sender;
//
//-(IBAction)closePressed:(id)sender;

@end
