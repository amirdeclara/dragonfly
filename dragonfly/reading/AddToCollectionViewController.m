//
//  AddToCollectionViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/22/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "AddToCollectionViewController.h"
#import "CollectionTableViewCell.h"
#import "Collection.h"
#import "HTTPSharedClient.h"

@interface AddToCollectionViewController ()

@end

@implementation AddToCollectionViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapBackground:)];
    [self.view addGestureRecognizer:gesture];
    
    [self loadCollectionData];
}


#pragma mark - Events

-(void) didTapBackground:(UITapGestureRecognizer *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.collections.count == 0) {
        return 0;
    }
    return self.collections.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collectionCell" forIndexPath:indexPath];
    [cell loadCollectData: [self.collections objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *collectionId = [self.collections objectAtIndex:indexPath.row][@"id"];
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    
    // NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    
    NSString *collectionPath = [NSString stringWithFormat:@"%@/collections/%@/posts", API_BASE_PATH, collectionId];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:collectionPath]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    NSString *escapedJSONString = [self.contentUrl stringByReplacingOccurrencesOfString:@"/" withString:@"\\/"];
    [parent setObject:escapedJSONString forKey:@"url"];
    [parent setObject:self.contentDescription forKey:@"description"];
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:parent
                                                       options:kNilOptions error:&error];
    
    
    [request setHTTPBody:postData];
    
    
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"Authorization"];
    //
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 [self dismissViewControllerAnimated:YES completion:nil];
                 // completion([Recommend createWithDict:jsonArray], nil);
             }
             //             completion([Profile createWithDict:jsonArray], nil);
             //            return;
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}

#pragma mark - UIViewController methods

-(void) loadCollectionData {
    
    
    //    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    //    NSLog(@"Value from accessToken: %@", accessToken);
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    NSString *collectionPath = [NSString stringWithFormat:@"%@/users/%@/collections?page=0&size=20", API_BASE_PATH, uuid];
    
    [[HTTPSharedClient sharedClient] GET:collectionPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //completion([Collection createWithDict:responseObject], nil);
        self.collections = responseObject[@"_embedded"][@"items"];
        LOG(@"self.collections is %@", self.collections);
        [self.collectionsTableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}

-(void) addCollection : (NSString *) collectionName{
    
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_KEY];
    
    //NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    
    NSString *feedPath = [NSString stringWithFormat:@"%@/collections", API_BASE_PATH];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:feedPath]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:collectionName forKey:@"title"];
    // [parent setObject:@"collection" forKey:@"type"];
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:parent
                                                       options:kNilOptions error:&error];
    
    [request setHTTPBody:postData];
    
    
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"DeclaraAuthorization"];
    //
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 [self dismissViewControllerAnimated:YES completion:nil];
                 // completion([Recommend createWithDict:jsonArray], nil);
             }
             //             completion([Profile createWithDict:jsonArray], nil);
             //            return;
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}

#pragma mark - IBAction

-(IBAction)createCollection:(id)sender {
    
    NSString *createCollectionPath = [NSString stringWithFormat:@"%@/collections", API_BASE_PATH];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:self.collectionsTextField.text forKey:@"title"];
    [parent setObject:@"false" forKey:@"closed"];
    LOG(@"The parent is %@", parent);
    
    [[HTTPSharedClient sharedClient] POST:createCollectionPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        [self dismissViewControllerAnimated:YES completion:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}

#pragma mark - UITextView Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    NSString *createCollectionPath = [NSString stringWithFormat:@"%@/collections", API_BASE_PATH];
    
    NSMutableDictionary *parent = [[NSMutableDictionary alloc] init];
    [parent setObject:self.collectionsTextField.text forKey:@"title"];
    [parent setObject:@"false" forKey:@"closed"];
    LOG(@"The parent is %@", parent);
    
    [[HTTPSharedClient sharedClient] POST:createCollectionPath parameters:parent success:^(AFHTTPRequestOperation *operation, id responseObject) {
        LOG(@"Success");
        //[self dismissViewControllerAnimated:YES completion:nil];
        [self loadCollectionData];
        self.collectionsTextField.text = @"";
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
    return YES;
}

@end
