//
//  ReadingArticleViewController.m
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ReadingArticleViewController.h"
#import "ReadingArticleTableViewCell.h"
#import "HTTPSharedClient.h"
#import "NSString+declara.h"
#import "ProgressHUD.h"
#import "AddToCollectionViewController.h"
#import "UIColor+declara.h"
#import "UIFont+declara.h"
#import "UITextView+declara.h"

@interface ReadingArticleViewController () {
    
    CAGradientLayer *maskLayer;
    NSMutableDictionary *contentData;
    NSArray *collection;
}

@property (nonatomic, copy) NSNumber *rowHeight;

@end

@implementation ReadingArticleViewController

#define FONT_SIZE 16.0f
#define CELL_CONTENT_WIDTH 335.0f
#define CELL_CONTENT_MARGIN 0.0f
#define CELL_BUFFER_HEIGHT 50
#define UITEXT_LINE_SPACE 8


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    contentData = [[NSMutableDictionary alloc] init];
    [self loadContentData];
    
    CGRect frame = self.closeView.frame;
    frame.origin.y = self.tableView.frame.size.height - self.closeView.frame.size.height;
    self.closeView.frame = frame;
    
    [self.view bringSubviewToFront:self.closeView];
    
    [self initialBackgroundColor];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}



-(void) loadContentData{
    [ProgressHUD show:@"Loading..."];
    NSString *contentPath=@"";
 
    contentPath = [NSString stringWithFormat:@"%@/contents/%@", API_BASE_PATH, self.contentId];
    
    [[HTTPSharedClient sharedClient] GET:contentPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [ProgressHUD dismiss];
        contentData = responseObject;
        collection = responseObject;
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize maximumLabelSize = CGSizeMake(self.tableView.frame.size.width, MAXFLOAT);
    
    NSStringDrawingOptions options = NSStringDrawingTruncatesLastVisibleLine |
    NSStringDrawingUsesLineFragmentOrigin;
    
    NSDictionary *attr = @{NSFontAttributeName: [UIFont defaultFont:18]};
    CGRect labelBounds = [[NSString stringByStrippingHTML:contentData[@"body"]] boundingRectWithSize:maximumLabelSize
                                                                                             options:options
                                                                                          attributes:attr
                                                                                             context:nil];
    CGFloat height = ceilf(labelBounds.size.height);
    LOG(@"The height is %f", height);
    return height+ CELL_BUFFER_HEIGHT;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReadingArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReadingArticleTableViewCell"];
    if(!cell) {
        cell = [[ReadingArticleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReadingArticleTableViewCell"];
    }
    
    if(contentData.count >0){
    
        NSMutableDictionary *regularFont = [[NSMutableDictionary alloc] init];
        [regularFont setObject:[UIFont defaultFont:17] forKey:NSFontAttributeName];
        NSMutableDictionary *boldFont = [[NSMutableDictionary alloc] init];
        [boldFont setObject:[UIFont defaultFontBold:20] forKey:NSFontAttributeName];
        
        NSMutableAttributedString *header = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", contentData[@"title"]] attributes:boldFont];
        NSMutableAttributedString *body = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n%@", [NSString stringByStrippingHTML:contentData[@"body"]]] attributes:regularFont];
        
        [header appendAttributedString:body];
        [cell.contentBody setAttributedText:header];

        cell.contentBody.layoutManager.delegate = self;
        CGSize textSize = [[header string] sizeWithFont:[UIFont fontWithName:@"Arial" size:20.0]
                                   constrainedToSize:CGSizeMake(320.0f, 99999.0f)
                                       lineBreakMode:UILineBreakModeWordWrap];
        CGRect frame = cell.contentBody.frame;
        frame.size = textSize;
        cell.contentBody.frame = frame;
        
    }

    return cell;
}

- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    return UITEXT_LINE_SPACE;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[AddToCollectionViewController class]])
    {
        ((AddToCollectionViewController*)segue.destinationViewController).contentUrl = contentData[@"source_url"];
        ((AddToCollectionViewController*)segue.destinationViewController).contentDescription = contentData[@"description"];
    }
}

#pragma mark Collection view scrolling

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    NSArray *colors;
    
    if (scrollView.contentOffset.y + scrollView.contentInset.top >= 0) {
        colors = @[(__bridge id)innerColor, (__bridge id)innerColor,
                   (__bridge id)innerColor, (__bridge id)outerColor];
    }
    else if (scrollView.contentOffset.y + scrollView.frame.size.height
             >= scrollView.contentSize.height) {
        colors = @[(__bridge id)outerColor, (__bridge id)innerColor,
                   (__bridge id)innerColor, (__bridge id)innerColor];
        
    }
    else {
        colors = @[(__bridge id)outerColor, (__bridge id)innerColor,
                   (__bridge id)innerColor, (__bridge id)outerColor];
    }
    ((CAGradientLayer *)scrollView.layer.mask).colors = colors;
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    scrollView.layer.mask.position = CGPointMake(0, scrollView.contentOffset.y);
    [CATransaction commit];
    
    [self layoutCloseButtonForScrollViewOffset:scrollView.contentOffset];
}

- (void)layoutCloseButtonForScrollViewOffset:(CGPoint)offset
{
    if (offset.y > 0)
    {
        self.closeButton.imageView.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * offset.y * 2);
        self.shareButton.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * offset.y * 2);
        self.collectButton.alpha =   1 - (1 / CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height).size.height * offset.y * 2);
        
        if(self.closeButton.imageView.alpha <= ALPHA_CLOSE_VIEW){
            self.closeButton.imageView.alpha = ALPHA_CLOSE_VIEW;
        }
        if(self.shareButton.alpha <= ALPHA_CLOSE_VIEW){
            self.shareButton.alpha = ALPHA_CLOSE_VIEW;
        }
        if(self.collectButton.alpha <= ALPHA_CLOSE_VIEW){
            self.collectButton.alpha = ALPHA_CLOSE_VIEW;
        }
    }
    else{
        self.closeButton.alpha = 1.0;
        
    }
}

#pragma mark - UIViewController methods

-(void) initialBackgroundColor {
    
    
    if (!self.tableView.layer.mask)
    {
        maskLayer = [CAGradientLayer layer];
        
        maskLayer.locations = @[[NSNumber numberWithFloat:0.0],
                                [NSNumber numberWithFloat:0.2],
                                [NSNumber numberWithFloat:0.8],
                                [NSNumber numberWithFloat:1.0]];
        
        maskLayer.bounds = CGRectMake(0, 0,
                                      self.tableView.frame.size.width,
                                      self.tableView.frame.size.height);
        maskLayer.anchorPoint = CGPointZero;
        
        self.tableView.layer.mask = maskLayer;
    }
    
    CGColorRef outerColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    CGColorRef innerColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
    NSArray *colors;
    
    colors = @[(__bridge id)innerColor, (__bridge id)innerColor,
               (__bridge id)innerColor, (__bridge id)outerColor];
    
    ((CAGradientLayer *)self.tableView.layer.mask).colors = colors;
    
}

#pragma mark - IBAction

-(IBAction)closeWebView:(id)sender{
    [ProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)shareContent:(id)sender {
    NSString *string = @"Please write something to share";
    UIActivityViewController *activityShareView = [[UIActivityViewController alloc] initWithActivityItems:@[string]
                                                                                    applicationActivities:nil];
    
    [self presentViewController:activityShareView
                       animated:YES
                     completion:^{
                         NSLog(@"completed.");
                     }];
    
}


-(IBAction)collectContent:(id)sender{
    [self performSegueWithIdentifier:@"AddtoCollectionsSegue" sender:self];
    
}


@end
