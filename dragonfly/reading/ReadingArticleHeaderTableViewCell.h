//
//  ReadingArticleHeaderTableViewCell.h
//  dragonfly
//
//  Created by Amirul Islam on 3/20/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadingArticleHeaderTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *headerTitle;
@property (nonatomic, strong) NSString *favTitle;

@end
