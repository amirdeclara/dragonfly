//
//  Profile.m
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Profile.h"
#import "HTTPSharedClient.h"

@implementation Profile


+ (Profile *) createWithDict:(NSDictionary *)data
{
    Profile *item = [[Profile alloc] init];
    
    LOG(@"The data is %@", data);
    
    if(data[@"name"] != [NSNull null]){
        item.firstName = data[@"name"][@"first"];
        item.lastName = data[@"name"][@"last"];
        item.fullName = data[@"name"][@"full"];
    }
    
    if(data[@"emails"] != [NSNull null]){
        item.primaryEmail = data[@"emails"][@"primary"];
    }
    
    if(data[@"image_url"] != [NSNull null]){
        item.imageUrl = data[@"image_url"];
    }
    
    if(data[@"total_followers_count"] != [NSNull null]){
        item.noFollowers = data[@"total_followers_count"];
    }
    if(data[@"total_collections_followed"] != [NSNull null]){
        item.noCollections = data[@"total_collections_followed"];
    }
    if(data[@"id"] != [NSNull null]){
        item.userId = data[@"id"];
    }
    
    return item;
}

+ (void) profileWithCompletion:(void (^)(id, NSError *))completion
{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    NSString *profilePath = [NSString stringWithFormat:@"%@/users/%@/profile", API_BASE_PATH, uuid ];
    
    [[HTTPSharedClient sharedClient] GET:profilePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Profile createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}



+ (void) loadProfile:(NSString *)profile accessToken:(NSString *) accessToken completion:(void (^)(id result, NSError *error))completion
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:profile]];
    [request setHTTPMethod:@"GET"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(jsonArray)
             {
                 completion([Profile createWithDict:jsonArray], nil);
                 return;
             }
             
             completion([NSArray array], nil);
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}


+ (NSArray *) createFromArray:(NSArray *)array
{
    NSMutableArray *outArray = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in array)
    {
        Profile *profile = [Profile createWithDict:dict];
        [outArray addObject:profile];
    }
    
    return outArray;
}


@end
