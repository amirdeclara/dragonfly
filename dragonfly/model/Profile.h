//
//  Profile.h
//  dragonfly
//
//  Created by Amirul Islam on 2/25/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *primaryEmail;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *noFollowers;
@property (nonatomic, strong) NSString *noCollections;
@property (nonatomic, strong) NSString *userId;


+ (Profile *) createWithDict:(NSDictionary *)data;
+ (void) loadProfile:(NSString *)profile accessToken:(NSString *) accessToken completion:(void (^)(id result, NSError *error))completion;
+ (void) profileWithCompletion:(void (^)(id, NSError *))completion;

@end
