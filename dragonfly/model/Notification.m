//
//  Notification.m
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Notification.h"
#import "HTTPSharedClient.h"

@implementation Notification


+ (Notification *) createWithDict:(NSDictionary *)data
{
    Notification *item = [[Notification alloc] init];
    
    LOG(@"The data is %@", data[@"content"]);
    item = data[@"content"];
    return item;
}

+ (void) notificationWithCompletion:(void (^)(id, NSError *))completion
{
    NSString *notificationPath = [NSString stringWithFormat:@"%@/notifications", API_BASE_PATH];
    
    LOG(@"Value from notificationPath: %@", notificationPath);
    
    [[HTTPSharedClient sharedClient] GET:notificationPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Notification createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}

+ (void) loadNotification:(NSString *)notification accessToken:(NSString *) accessToken completion:(void (^)(id result, NSError *error))completion {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:notification]];
    [request setHTTPMethod:@"GET"];
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@",accessToken] forHTTPHeaderField:AUTHORIZATION_HEADER];
    //
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSLog(@"NO Error: %@", data);
             NSError * serializationError = nil;
             NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializationError];
             
             if(serializationError == nil && jsonArray != nil) {
                 NSLog(@"%@", jsonArray);
                 completion([Notification createWithDict:jsonArray], nil);
             }
 
         }
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
}

@end
