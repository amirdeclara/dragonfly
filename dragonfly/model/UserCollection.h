//
//  UserCollection.h
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserCollection : NSObject 

@property(nonatomic, strong) NSString *collectionTitle;
@property(nonatomic, strong) NSMutableArray *collections;
@property(nonatomic, strong) NSMutableArray *owners;
@property(nonatomic, strong) NSString *followed;
@property(nonatomic, strong) NSString *collectionId;
@property(nonatomic, strong) NSString *profileImageUrl;

//+ (void) userCollectionWithCompletion:(void (^)(id, NSError *))completion;
+ (void) userCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion;

@end
