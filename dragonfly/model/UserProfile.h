//
//  UserProfile.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject


@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *primaryEmail;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *noFollowers;
@property (nonatomic, strong) NSString *noCollections;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *followed;

+ (UserProfile *) createWithDict:(NSDictionary *)data;
+ (void) userProfileWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion;

@end
