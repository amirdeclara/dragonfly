//
//  UserProfile.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UserProfile.h"
#import "HTTPSharedClient.h"

@implementation UserProfile


+ (UserProfile *) createWithDict:(NSDictionary *)data
{
    UserProfile *item = [[UserProfile alloc] init];
    
    LOG(@"The data is %@", data);
    
    if(data[@"name"] != [NSNull null]){
        item.firstName = data[@"name"][@"first"];
        item.lastName = data[@"name"][@"last"];
        item.fullName = data[@"name"][@"full"];
    }
    
    if(data[@"emails"] != [NSNull null]){
        item.primaryEmail = data[@"emails"][@"primary"];
    }
    
    if(data[@"image_url"] != [NSNull null]){
        item.imageUrl = data[@"image_url"];
    }
    
    if(data[@"total_followers_count"] != [NSNull null]){
        item.noFollowers = data[@"total_followers_count"];
    }
    if(data[@"total_collections_followed"] != [NSNull null]){
        item.noCollections = data[@"total_collections_followed"];
    }
    if(data[@"id"] != [NSNull null]){
        item.userId = data[@"id"];
    }
    if(data[@"followed"] != [NSNull null]){
        item.followed = [data[@"followed"] stringValue];
    }
    return item;
}

+ (void) userProfileWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion
{
    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([UserProfile createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

@end
