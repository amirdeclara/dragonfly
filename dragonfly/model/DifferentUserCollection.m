//
//  DifferentUserCollection.m
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "DifferentUserCollection.h"
#import "HTTPSharedClient.h"

@implementation DifferentUserCollection

+ (DifferentUserCollection *) createWithDict:(NSDictionary *)data
{
    DifferentUserCollection *item = [[DifferentUserCollection alloc] init];
    item = data[@"_embedded"][@"items"];
    return item;
}


+ (void) DifferentUserCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion
{    
    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([DifferentUserCollection createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}


@end
