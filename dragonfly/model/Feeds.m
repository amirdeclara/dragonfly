//
//  Feeds.m
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Feeds.h"
#import "HTTPSharedClient.h"

@implementation Feeds


+ (Feeds *) createWithDict:(NSDictionary *)data
{
    Feeds *item = [[Feeds alloc] init];
    
    LOG(@"The data is %@", data[@"_embedded"][@"items"]);
    item = data[@"_embedded"][@"items"];
    
    return item;
}


+ (void) feedWithCompletion:(void (^)(id, NSError *))completion
{

    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];

    NSString *feedPath = [NSString stringWithFormat:@"%@/users/%@/recommendations?page=0&size=20&type=post&category=social", API_BASE_PATH, uuid];
    
    [[HTTPSharedClient sharedClient] GET:feedPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Feeds createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}


@end
