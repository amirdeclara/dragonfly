//
//  DifferentUserCollection.h
//  dragonfly
//
//  Created by Amirul Islam on 3/23/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DifferentUserCollection : NSObject

+ (void) DifferentUserCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion;

@end
