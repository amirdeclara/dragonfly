//
//  Content.h
//  dragonfly
//
//  Created by Amirul Islam on 3/10/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Content : NSObject {
    
    Content *content;
}

@property (nonatomic, strong) NSString *contentBody;
@property (nonatomic, strong) NSString *contentImageUrl;
@property (nonatomic, strong) NSString *contentTitle;

+ (void) contentDetailWithCompletion:(NSString *)path completion:(void (^)(id, NSError *))completion;
- (NSString *)bodyContent;

@end
