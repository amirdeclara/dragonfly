//
//  ArticleCollection.h
//  dragonfly
//
//  Created by Amirul Islam on 3/18/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleCollection : NSObject

@property(nonatomic, strong) NSString *collectionTitle;
@property(nonatomic, strong) NSMutableArray *collections;
@property(nonatomic, strong) NSMutableArray *owners;
@property(nonatomic, strong) NSString *followed;
@property(nonatomic, strong) NSString *profileImageUrl;

//+ (void) userCollectionWithCompletion:(void (^)(id, NSError *))completion;
+ (void) articleCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion;

@end
