//
//  Notification.h
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject


+ (Notification *) createWithDict:(NSDictionary *)data;

+ (void) loadNotification:(NSString *)notification accessToken:(NSString *) accessToken completion:(void (^)(id result, NSError *error))completion;

+ (void) notificationWithCompletion:(void (^)(id, NSError *))completion;

@end
