//
//  Recommend.h
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recommend : NSObject

+ (Recommend *) createWithDict:(NSDictionary *)data;
+ (void) recommendWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion;

@end
