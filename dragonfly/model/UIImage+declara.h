//
//  UIImage+declara.h
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (declara)

-(void) roundCornerView:(UIImageView *)view;

-(void) circularImage:(UIImageView *)view radius:(float) radius;

- (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;

@end
