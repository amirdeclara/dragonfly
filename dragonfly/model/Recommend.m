//
//  Recommend.m
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Recommend.h"
#import "HTTPSharedClient.h"

@implementation Recommend

+ (Recommend *) createWithDict:(NSDictionary *)data
{
    Recommend *item = [[Recommend alloc] init];
    
    LOG(@"The data is %@", data[@"_embedded"][@"items"]);
    item = data[@"_embedded"][@"items"];
    
    return item;
}

+ (void) recommendWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion
{

    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Recommend createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}

@end
