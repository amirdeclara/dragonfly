//
//  UserCollection.m
//  dragonfly
//
//  Created by Amirul Islam on 3/16/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "UserCollection.h"
#import "HTTPSharedClient.h"

@implementation UserCollection


+ (UserCollection *) createWithDict:(NSDictionary *)data
{
    UserCollection *item = [[UserCollection alloc] init];
    LOG(@"The data is %@", item.collections);
    item.collectionTitle = data[@"title"];
    item.collections = data[@"_embedded"][@"posts"][@"_embedded"][@"items"];
    item.owners = data[@"_embedded"][@"owners"];
    item.followed = [data[@"followed"] stringValue];
    item.collectionId = data[@"id"];
    item.profileImageUrl = data[@"_embedded"][@"owners"][0][@"image_url"];
    return item;
}


+ (void) userCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion
{

    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([UserCollection createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}


@end
