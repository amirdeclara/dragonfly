//
//  Content.m
//  dragonfly
//
//  Created by Amirul Islam on 3/10/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Content.h"
#import "HTTPSharedClient.h"

@implementation Content


+ (Content *) createWithDict:(NSDictionary *)data
{
    Content *item = [[Content alloc] init];
    
    LOG(@"The data is %@", data);
    
    if(data[@"title"] != [NSNull null]){
        item.contentTitle = data[@"title"];
    }
    
    if(data[@"image_url"] != [NSNull null]){
        item.contentImageUrl = data[@"image_url"];
    }
    
    if(data[@"body"] != [NSNull null]){
        item.contentBody = data[@"body"];
    }
    
    return item;
}

- (NSString *)bodyContent{
    
    NSString *bodyString = content.contentBody;
    return bodyString;
}

+ (void) contentDetailWithCompletion:(NSString *)path completion:(void (^)(id, NSError *))completion
{
    
    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Content createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}


@end
