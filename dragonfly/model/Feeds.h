//
//  Feeds.h
//  dragonfly
//
//  Created by Amirul Islam on 2/27/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feeds : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *primaryEmail;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *imageUrl;


+ (void) feedWithCompletion:(void (^)(id, NSError *))completion;

@end
