//
//  Collection.h
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Collection : NSObject

+ (void) collectionWithCompletion:(void (^)(id, NSError *))completion;

@end
