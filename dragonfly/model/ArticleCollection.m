//
//  ArticleCollection.m
//  dragonfly
//
//  Created by Amirul Islam on 3/18/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "ArticleCollection.h"
#import "HTTPSharedClient.h"

@implementation ArticleCollection

+ (ArticleCollection *) createWithDict:(NSDictionary *)data
{
    ArticleCollection *item = [[ArticleCollection alloc] init];
    
    item.collectionTitle = data[@"_embedded"][@"content"][@"title"];
    item.collections = data[@"_embedded"][@"content"][@"_embedded"][@"posts"][@"_embedded"][@"items"];
    item.owners = data[@"_embedded"][@"content"][@"author_name"];
    item.followed = [data[@"followed"] stringValue];
    item.profileImageUrl = data[@"_embedded"][@"owners"][0][@"image_url"];
    return item;
}


+ (void) articleCollectionWithCompletion:(NSString *) path completion:(void (^)(id, NSError *))completion
{
    [[HTTPSharedClient sharedClient] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([ArticleCollection createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
}


@end
