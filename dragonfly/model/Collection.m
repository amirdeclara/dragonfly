//
//  Collection.m
//  dragonfly
//
//  Created by Amirul Islam on 3/5/15.
//  Copyright (c) 2015 declara. All rights reserved.
//

#import "Collection.h"
#import "HTTPSharedClient.h"

@implementation Collection


+ (Collection *) createWithDict:(NSDictionary *)data
{
    Collection *item = [[Collection alloc] init];
    
    LOG(@"The data is %@", data[@"_embedded"][@"items"]);
    item = data[@"_embedded"][@"items"];
    return item;
}


+ (void) collectionWithCompletion:(void (^)(id, NSError *))completion
{
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:UUID_TOKEN_KEY];
    
    NSString *collectionPath = [NSString stringWithFormat:@"%@/users/%@/collections?page=0&size=20", API_BASE_PATH, uuid];

    [[HTTPSharedClient sharedClient] GET:collectionPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        completion([Collection createWithDict:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        LOG(@"The error is %@", error);
    }];
    
}



@end
